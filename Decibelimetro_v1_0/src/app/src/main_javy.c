
//GPIO del Keypad
//GPIO buttonMeasure;
//GPIO buttonCalibrate;
//GPIO buttonCurve;
//
////LCD
//LCDtext MyLCD;
//
//void setupButtonsAndLCD(void);
//void LCDWriteString(char * str);
//
//int main(void)
//{
//	uint32_t i, j ;
//	//Configuracion de LCD y botones de user input
////	setupButtonsAndLCD();
////
////	LCDWriteString("Tecla Enter");
//
//	GPIO_Init(&(MyLCD.RS), SALIDA, BAJO, (int)LCD_RS);
//
//	while(1)
//	{
//		AsignarNivel(&(MyLCD.RS), BAJO);
//		Activar(&(MyLCD.RS));
//		for(i=0;i<1000000;i++)
//		{
//			for(j=0;j<100;j++)
//			{
//
//			}
//		}
//
//		Pasivar(&(MyLCD.RS));
//		for(i=0;i<1000000;i++)
//		{
//			for(j=0;j<100;j++)
//			{
//
//			}
//		}
//	}
//
//
////	xTaskCreate( 	MenuLCD,
////					"MenuLCD",
////					configMINIMAL_STACK_SIZE,
////					NULL,
////					tskIDLE_PRIORITY+1,
////					NULL);
////
////	xTaskCreate(	TeclaEvent,
////					"Tecl",
////					configMINIMAL_STACK_SIZE,
////					NULL,
////					tskIDLE_PRIORITY+2,
////					NULL );
////
////	SemaforoLCD=xSemaphoreCreateMutex();
//
//	while(1);
//
//	return 0;
//}
//
//void LCDWriteString(char * str)
//{
//	LCDtextClear(&MyLCD);
//	LCDtextGotoXY(&MyLCD,0,0);
//	int i;
//	for(i=0; i<strlen(str); i++)
//		LCDtextPutchar(&MyLCD, (int) str[i]);
//}
//
//void setupButtonsAndLCD(void)
//{
//	//Inicializacion del LCD
//	LCDtextInit(&MyLCD, LCD_ROWS, LCD_COLUMNS, LCD_DB4, LCD_DB5, LCD_DB6, LCD_DB7, LCD_RS, LCD_EN);
//	//Botones para nuestra placa
//	GPIO_Init(&buttonMeasure, ENTRADA, BAJO, (int)GPIO__0_1);
//	GPIO_Init(&buttonCalibrate, ENTRADA, BAJO, (int)GPIO__0_18);
//	GPIO_Init(&buttonCurve, ENTRADA, BAJO, (int)GPIO__0_17);
//}


//void TeclaEvent (void *pvParameters)
//{
//	//GPIO *pin;
//	unsigned int EstadoDebounce = NO_OPRIMIDO;
//	TECLA tecla;
//	portTickType xMeDesperte;
//	int bufferTecla=BOTON_NO_PRESIONADO;
//
//	//Inicio de variables y recuperación de parámetros
//	//pin = (GPIO*)(pvParameters);
//	xMeDesperte = xTaskGetTickCount();
//	tecla.codigo = BOTON_NO_PRESIONADO;
//	tecla.tiempo = 0;
//
//	while(1)
//	{
//	//debo verificar rebote
//	switch(EstadoDebounce)
//	{
//		case NO_OPRIMIDO:
//			vTaskDelayUntil(&xMeDesperte,SCAN_RATE_ms/portTICK_RATE_MS);
//			bufferTecla=BarrerKeypad();
//			if(bufferTecla!=BOTON_NO_PRESIONADO)	//Si retorna una opresión
//				EstadoDebounce = DEBOUNCE;		//Indico que esta corriendo el tiempo Debounce
//			break;
//
//		case DEBOUNCE:
//			vTaskDelay(TIEMPO_DE_DEBOUNCE_ms/portTICK_RATE_MS);
//			EstadoDebounce = VALIDAR;
//
//		case VALIDAR:
//			if(bufferTecla==BarrerKeypad())			//Si retorna algo sigue presionado
//			{
//				EstadoDebounce=OPRIMIDO;
//				tiempo_de_pulsacion = 0;
//			}
//			else							// fue error
//				EstadoDebounce=NO_OPRIMIDO;
//			break;
//
//		case OPRIMIDO:
//			if(BarrerKeypad()==BOTON_NO_PRESIONADO) //No envio mensaje hasta no soltar
//			{
//				tecla.codigo = bufferTecla;
//				tecla.tiempo = tiempo_de_pulsacion;
//				tiempo_de_pulsacion = -1;
//				//Envío a la cola, optamos por no bloquear si está llena
//				xQueueSend( xQueueKeypad, &tecla, 0 );
//				EstadoDebounce = NO_OPRIMIDO;
//			}
//			break;
//
//		default:
//			break;
//	}
//	}
//}

//void MenuLCD (void *pvParameters)
//{
//	portTickType xMeDesperte;
//	xMeDesperte = xTaskGetTickCount();
//	TECLA tecla;
//
//	configPredeterminada();
//
//
//
//	while(1)
//	{
//
//		xQueueReceive(xQueueKeypad, &tecla, portMAX_DELAY);
//
//
//		//Prueba de los botones
//		switch(tecla.codigo)
//		{
//			case ENTER:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Tecla Enter");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case PLAY:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Tecla Play");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case MENU:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Tecla Menu");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case NEXT:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Tecla Next");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case PREVIOUS:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Tecla Previous");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case VOLUMEUP:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Volume up");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//			case VOLUMEDOWN:
//				Toggle_Led();
//				xSemaphoreTake(SemaforoLCD,portMAX_DELAY);
//				LCDWriteString("Volume down");
//				xSemaphoreGive(SemaforoLCD);
//				break;
//		}
//
//	}
//}
