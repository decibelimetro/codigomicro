#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>

//Standard includes
#include	"stdlib.h"
#include	"stdint.h"
#include	"string.h"

//API Includes
#include	"appStateMachine.h"
#include	"delay.h"
#include	"lcd.h"


int main()
{
	static const char initialMessage[] = "   Sonometro                              UTN FRBA MEI";
	//Configuro el delay para 1ms
	delayConfig(1);

	//Configuro el LCD
	Config_Puerto();
	//Inicializo el display
	IniciarLCD(1,0,1,0,1,1,0/*1,0,1,0,0,1,0*/);
	//Inicializo el display
	IniciarLCD(1,0,1,0,1,1,0/*1,0,1,0,0,1,0*/);

	//Escribo un mensaje inicial
	Escribir_String(initialMessage);

	//Configuro las entradas
	GPIO_SetDir(APP_COMMANDS_PORT, 1 << CALIBRATE_BUTTON_PIN, 0);
	GPIO_SetDir(APP_COMMANDS_PORT, 1 << MEASURE_BUTTON_PIN, 0);
	GPIO_SetDir(APP_COMMANDS_PORT, 1 << CURVE_BUTTON_PIN, 0);

	while(1)
	{
		appStateMachine();
	}

	while(1)
	{

	}
	return 0;
}


