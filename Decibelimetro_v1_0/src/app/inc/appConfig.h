#ifndef		APPCONFIG_H
	#define		APPCONFIG_H

/**
 * @def		SAMPLES
 * @brief	It is the number of samples to do.
 */
#define		SAMPLES					(4*1024)

/**
 * @def		FFT_LENGHT
 * @brief	It is the length of the Fast Fourier Transform.
 */
#define 	FFT_LENGHT (SAMPLES	/2)

/**
 * @def		SAMPLE_PERIOD_USEC
 * @brief	It is the sample period.
 */
#define		SAMPLE_PERIOD_USEC		42
//si ponemos un período de 37us no nos queda más acorde a la norma? 31us*4096 samples= 126.976ms -- 42us*4096 samples=172.032 ms Le da el cuero al ADC?

/**
 * @def		TIMER
 * @brief	It is the selected timer for operating the acquisition from the adc.
 */
#define		TIMER					(LPC_TIM0)
/**
 * @def		TIMER_MATCH_CHANNEL
 * @brief	It is the channel match channel to do the toggle of the pin in order to run the conversions periodically.
 */
#define		TIMER_MATCH_CHANNEL		0

/**
 * @def		LCD_ROWS
 * @brief	Es el numero filas del display
 */
#define		LCD_ROWS				2
/**
 * @def		LCD_COLUMNS
 * @brief	Es el numero de columnas
 */
#define		LCD_COLUMNS				16

/**
 * @def		DEBUG_MODE
 * @brief	Modo debug de compilacion o modo de programa.
 */
//#define		DEBUG_MODE		1

#endif
