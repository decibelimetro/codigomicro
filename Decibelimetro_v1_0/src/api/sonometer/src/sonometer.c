#include "sonometer.h"

//Variables publicas

volatile uint32_t decInputSignalSize = 0;
volatile uint32_t decInputSignalIndex = 0;
volatile float *decInputSignal = NULL;
const float *decOutputBuffer = (float *)0x2007C000;
volatile decAcquireEState decAcquireState = DEINITIALIZED;
volatile ADC_dev adcDevice;

//Funciones publicas

decAcquireEState decAcquire(float *inputBuffer, uint32_t size)
{
	static decAcquireEState state = DEINITIALIZED;

	switch(state)
	{
		case DEINITIALIZED:
			//Configuro el timer
			decConfigTimerForConversion();
			//Configuro los indices
			decInputSignalIndex = 0;
			decInputSignalSize = size;
			decInputSignal = inputBuffer;
			//Paso de estado a processing
			state = PROCESSING;
			//Coloco el estado en procesando
			decAcquireState = PROCESSING;
			//Inicializo el ADC
			adcInit(&adcDevice);
			//Inicio la conversion
			decInitTimerForConversion();
			break;
		case FINISHED:
			//Finalizamos de adquirir, pasamos de estado a
			//desinicializado para la proxima conversion
			state = DEINITIALIZED;
			break;
		case PROCESSING:
			//¿Terminamos de convertir?
			if(decAcquireState == FINISHED)
			{
				//Paramos el timer
				decDeInitTimerForConversion();
				//Como terminamos de convertir pasamos a finalizado
				state = FINISHED;
			}
			//No terminamos de convertir por lo que seguimos procesando
			break;
		default:
			//Caimos en un estado invalido
			//Frenamos el timer
			decDeInitTimerForConversion();
			//Pasamos al estado desinicializado
			state = DEINITIALIZED;
			break;
	}
	return state;
}

float decProcessPower(float *inputBuffer,
					uint32_t inputSize,
					float *outputBuffer,
					uint32_t outputSize,
					decEWeightingFilters weightingCurve)
{
	uint32_t i;
	float power;

	//Realizo la copia del vector de la señal de entrada al de la salida para luego aplicarle la FFT
	outputBuffer[0] = 0.0;
	for(i = 1; i <= inputSize; i++)
	{
		outputBuffer[i] = inputBuffer[i-1];
	}

	//Aplico la ventana Flat-Top a la señal en tiempo
	dspMatrix_ScalarMultiplication(&(outputBuffer[1]), windowFlatTop, &(outputBuffer[1]),  outputSize - 1);

	//Aplico la FFT
	dspRealFFT(outputBuffer, outputSize - 1, 1);

	//4.- Se aplica el filtro de ponderacion.
	switch (weightingCurve)
	{
		case CURVE_NONE:
			//No se debe multiplicar por ninguna ponderacion espectral al resultado de la FFT.
			break;

		case CURVE_A:
			dspMatrix_ScalarMultiplication(outputBuffer, FiltroA, outputBuffer, outputSize - 1);
		break;

		case CURVE_B:
			dspMatrix_ScalarMultiplication(outputBuffer, FiltroB, outputBuffer, outputSize - 1);
		break;

		case CURVE_C:
			dspMatrix_ScalarMultiplication(outputBuffer, FiltroC, outputBuffer, outputSize - 1);
		break;

		case CURVE_D:
			dspMatrix_ScalarMultiplication(outputBuffer, FiltroD, outputBuffer, outputSize - 1);
		break;

		default:
			//Error, no hay tipo de curva que no haya sido tenida en cuenta
			return 0.0;
			break;
	}

	//Obtengo la potencia de la señal
	power = dspPower_Parseval(&outputBuffer[1], outputSize-1);
	//Normalizo la potencia por la ventana y la escala de la FFT
	power = 2.0 * power / (windowFlatTopPower * (float)(outputSize - 1) * (float)(outputSize - 1));

	//Multiplicar por la escala
	return power;
}


float decConvertPowerToDBPSL(float power)
{
	//Aplicar el logaritmo
	return 10.0 * log10(power) + SONOMETER_CONSTANT;
}

void decConfigTimerForConversion()
{
	//Estructuras de configuracion del timer
	TIM_TIMERCFG_Type timerCfg;
	//Estructura de configuracion del match
	TIM_MATCHCFG_Type matchCfg;

	//Configuracion del Timer
	timerCfg.PrescaleOption = TIM_PRESCALE_USVAL;
	timerCfg.PrescaleValue = 1;//1us
	//Configuracion del match
	matchCfg.MatchChannel = TIMER_MATCH_CHANNEL;
	matchCfg.MatchValue = SAMPLE_PERIOD_USEC / 2;	//Divido por dos ya que debo colocar un pulso para que inicie la conversion.
	matchCfg.IntOnMatch = ENABLE;
	matchCfg.ResetOnMatch = ENABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;

	//Inicio el timer
	TIM_Init(TIMER, TIM_TIMER_MODE, &timerCfg);
	TIM_ConfigMatch(TIMER, &matchCfg);
	//Habilito las interrupciones
	NVIC_EnableIRQ(TIMER0_IRQn);
}

inline void decInitTimerForConversion()
{
	//Habilito las interrupciones
	NVIC_EnableIRQ(TIMER0_IRQn);
	//Arranco el timer
	TIM_Cmd(TIMER, ENABLE);
}

inline void decDeInitTimerForConversion()
{
	//Habilito las interrupciones
	TIM_Cmd(TIMER, DISABLE);
}

void TIMER0_IRQHandler()
{
	if(decInputSignalIndex < decInputSignalSize)
	{
		//Comienzo la conversion
		adcCONVStart(&adcDevice);
		//Bloqueo hasta tener la conversion del ADC
		while(adcGetData(&adcDevice) != ADC_END);
		//Coloco el dato como float y lo envio al vector
		decInputSignal[decInputSignalIndex] = adcDevice.BUFFER;
		//Incremento el indice de muestras
		decInputSignalIndex++;
	}
	else
	{
		NVIC_DisableIRQ(TIMER0_IRQn);
		decAcquireState = FINISHED;
	}
}
