#ifndef		SONOMETER_H
	#define		SONOMETER_H

//Standard includes
#include <stdlib.h>
#include <string.h>

//CMSIS includes
#include "lpc17xx_gpio.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_pinsel.h"

//API includes
#include "ADC_External.h"
#include "dsp.h"
#include "flatTop.h"
#include "weightingFilters.h"

//Config includes
#include "appConfig.h"

/**
 * @def		SONOMETER_CONSTANT
 * @brief	Valor de potencia constante independiente de la tensión de medida.
 */
#define		SONOMETER_CONSTANT		((float) 103.9731417)

/**
 * @def		CALIBRATION_MEASUREMENTS
 * @brief	Numero de mediciones a realizar para calibrar
 */
#define		SONOMETER_CALIBRATION_MEASUREMENTS	100

/**
 * @def		CALIBRATION_MEASURE
 * @brief	Valor de calibracion que se debe medir al calibrar
 */
#define		SONOMETER_CALIBRATION_MEASURE			94.0


//10^((94-103.9731417)/10)
/**
 *
 */
#define		SONOMETER_PRESSURE_POWER_CALIBRATION	0.1006203515



//Enums

/**
 * @enum		decAcquireEState
 * @brief		Acquisition status
 */
typedef enum decAcquireEnumState
{
	DEINITIALIZED = 0,		/**< El timer esta desinicializado y no se estan adquiriendo muestras. */
	PROCESSING = 1,			/**< El microcontrolador esta adquiriendo muestras. */
	FINISHED = 2,			/**< Se finalizo con la adquisicion solicitada */
}decAcquireEState;

//Public functions
/**
 * @fn 		decAcquire()
 * @brief	It configures the timer in order to start adquiring all the samples
 * @param	inputBuffer		it is the buffer where to start putting the samples
 * @param	size			it is the number of samples to acquire
 * @return	the state of the acquisition process
 */
decAcquireEState decAcquire(float *inputBuffer, uint32_t size);

/**
 * @fn 		decProcessPower(float *inputSignal, uint32_t size, decEWeightingFilters weightingCurve)
 * @brief	It calculates the power appliyng the weighting filter to the input signal.
 * @param   inputSignal		it is the input buffer for the signal to calculate the power.
 * @param	size			it is the inputSignal buffer size.
 * @param	weightingCurve	it is the selected weighting filter to apply.
 * @return	the total power calculated.
 * @details	Los pasos a seguir en el calculo son:
 * 			1.- Se multiplica a la señal en tiempo por la ventana.
 * 			2.- Se calcula la FFT de dicha señal.
 * 			3.- Se calcula el modulo de la FFT.
 * 			4.- Se aplica el filtro de ponderacion.
 * 			5.- Se realiza el calculo de energia.
 * 			6.- Se realizan los calculos de ajuste de escala.
 * 			7.- Se aplica logaritmo
 */
float decProcessPower(float *inputBuffer,
					uint32_t inputSize,
					float *outputBuffer,
					uint32_t outputSize,
					decEWeightingFilters weightingCurve);

/**
 * @fn	decConvertPowerToDBPSL(float power)
 * @brief	Convierte potencia de tension a presion sonora
 * @param	power potencia a convertir a presion
 * @retval	Presion en dBPSL
 */
float decConvertPowerToDBPSL(float power);

//Private functions
/**
 * @fn			decConfigTimerForConversion()
 * @brief		It configures the timer for generating the pulses for the ADC
 */
void decConfigTimerForConversion();
/**
 *  @fn			decInitTimerForConversion()
 *  @brief		It inits the timer for starting to convert.
 */
inline void decInitTimerForConversion();
/**
 * @fn			decDeInitTimerForConversion()
 * @brief		Desinicializa el timer para las conversiones frenandolo
 */
inline void decDeInitTimerForConversion();


//Variables publicas
/**
 * @var		decInputSignalSize
 * @brief	Tamaño de la señal de entrada
 */
extern volatile uint32_t decInputSignalSize;
/**
 * @var		decInputSignalIndex
 * @brief	Indice de la señal de entrada
 */
extern volatile uint32_t decInputSignalIndex;
/**
 * @var		decInputSignal
 * @brief	Vector de la señal de entrada
 */
extern volatile float *decInputSignal;
/**
 * @var		decOutputBuffer
 * @brief	Vector de la señal de salida
 */
extern const float *decOutputBuffer;
/**
 * @var		decAcquireState
 * @brief	Estado inicial del proceso de adquisicion
 */
extern volatile decAcquireEState decAcquireState;
/**
 * @var		adcDevice
 * @brief	Dispositivo ADC utilizado
 */
extern volatile ADC_dev adcDevice;

#endif
