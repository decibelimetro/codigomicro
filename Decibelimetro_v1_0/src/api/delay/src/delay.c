#include "delay.h"

//Variables privadas
/**
 * @var	delayCount
 * @brief	Contador de milisegundos a bloquear
 */
static volatile uint32_t delayCount;

//Funciones publicas

void delayConfig()
{
//	//Configuro el systcick para un milisegundo
//	SYSTICK_InternalInit(1);
//	//Habilito el Sysctick
//	SYSTICK_Cmd(ENABLE);
//	//Habilito las interrupciones del systick
//	SYSTICK_IntCmd(ENABLE);
//	NVIC_EnableIRQ(SysTick_IRQn);
}

void delay(uint32_t milisec)
{
	uint32_t i;

	for(; milisec != 0; milisec --)
	{
		for(i = 0; i < 9090; i++);
	}

//	delayCount = milisec;
//
//	while(delayCount != 0)
//	{
//	}
}

//Funciones privadas
void SysTick_Handler(void)
{
//	if(delayCount != 0)
//	{
//		delayCount --;
//	}
}
