#ifndef		DELAY_H
	#define		DELAY_H

//Includes

//Standard includes
#include 	"stdint.h"

//API Includes
#include	"lpc17xx_systick.h"

//Public functions
/**
 * @fn		delayConfig()
 * @brief	Configuro el systick
 */
void delayConfig();
/**
 * @fn		delay(uint32_t milisec)
 * @brief	Delay bloqueante en milisegundos
 * @param	milisec es el tiempo en milisegundos que se desea demorar
 */
void delay(uint32_t milisec);

//Private functions
void SysTick_Handler(void);

#endif
