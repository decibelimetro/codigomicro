#include "utils.h"

//Funciones publicas

void printFloatSignal(float *x, uint32_t size, char *name)
{
	uint32_t i;
	//Imprimo sus valores
	printf("%s = [", name);
	for(i = 0; i < (size-1); i ++)
	{
		printf("%f;", x[i]);
	}
	printf("%f];\n\r",  x[size-1]);
}

void printFloatComplexSignal(float *x, uint32_t size, char *name)
{
	uint32_t i;
	uint32_t midSize = size/2;

	//Imprimo sus valores
	printf("%s = [", name);
	for(i = 0; i < midSize-1; i ++)
	{
		printf("%f + %fi;", x[2*i], x[2*i + 1]);
	}
	printf("%f + %fi];\n\r", x[2*(midSize - 1)], x[2*(midSize - 1) + 1]);
}

void createSin(float amplitude, float fSampling, float fSine, uint32_t size, float *buffer)
{
	uint32_t i;

	for(i = 0; i < size; i ++)
	{
		buffer[i] = amplitude * sinf(2 * PI * fSine * (float)i / fSampling);
	}
}
