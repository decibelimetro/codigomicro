#ifndef		UTILS_H
	#define		UTILS_H

//Standard Includes
#include	"stdlib.h"
#include	"stdint.h"
#include	"stdio.h"
#include 	"math.h"
//API Includes
#include	"appConfig.h"

//Define
#define		PI			3.14159

//Funciones publicas

/**
 * @fn printFloatSignal
 * @brief Imprime una señal para el matlab
 * @param x
 * @param size
 * @param name
 */
void printFloatSignal(float *x, uint32_t size, char *name);
/**
 * @fn printFloatComplexSignal
 * @brief Imprime una señal para el matlab
 * @param x
 * @param size
 * @param name
 */
void printFloatComplexSignal(float *x, uint32_t size, char *name);
/**
 * @fn getSine
 * @brief Obtengo un seno
 * @param amplitude Amplitud del seno
 * @param fSampling
 * @param fSine
 * @param size
 * @param buffer
 */
void createSin(float amplitude, float fSampling, float fSine, uint32_t size, float *buffer);

#endif
