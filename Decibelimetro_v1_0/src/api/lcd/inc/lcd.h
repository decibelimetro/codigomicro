#ifndef		LCD_H
	#define		LCD_H

//Includes
//Standard Includes
#include	"stdint.h"

//CMSIS Includes
#include	"lpc17xx_gpio.h"

//API Includes
#include	"delay.h"

//Defines
/**
 * @def		LCD_D4
 * @brief	Bit de datos 4
 */
#define LCD_D4 (1<<25)  //GPIO P0.25
/**
 * @def		LCD_D5
 * @brief	Bit de datos 5
 */
#define LCD_D5 (1<<26)  //GPIO P0.26
/**
 * @def		LCD_D6
 * @brief	Bit de datos 6
 */
#define LCD_D6 (1<<30)  //GPIO P1.30
/**
 * @def		LCD_D7
 * @brief	Bit de datos 7
 */
#define LCD_D7 (1<<31)  //GPIO P1.31
/**
 * @def		LCD_RS
 * @brief	Bit de datos RS
 */
#define LCD_RS (1<<23)  //GPIO P0.23
/**
 * @def		LCD_EN
 * @brief	Bit de enable
 */
#define LCD_EN (1<<24)  //GPIO P0.24
/**
 * @def		PORT_EN
 * @brief	Puerto de la linea de enable
 */
#define PORT_EN 0 //PORT 0
/**
 * @def		PORT_RS
 * @brief	Puerto de la linea RS
 */
#define PORT_RS 0 //PORT 0
/**
 * @def		PORT_BUS
 * @brief	Puerto de las lineas del bus
 */
#define PORT_BUS	0 //PORT 0

/**
 * @def		Delay_1mS
 * @brief	Delay de entre 1ms a 2 ms
 */
#define Delay_1mS		delay(30)
/**
 * @def		Delay_2mS
 * @brief	Delay de entre 2 a 3 ms
 */
#define Delay_2mS		delay(100)

/**
 * @def		Delay_500mS
 * @brief	Delay de arranque
 */
#define Delay_43mS		delay(1000)

/**
 * @def		Delay_43ms
 * @brief	Delay de entre 43 a 44 ms
 */
#define	 Delay_43mS		delay(1000)
/**
 * @def		Delay_43_uS
 * @brief	Delay de entre 1 y 2 ms.
 */
#define	 Delay_43_uS 	delay(100)
/**
 * @def		Borra_todo
 * @brief	Pone todos los pines de datos en estado bajo.
 */
#define Borra_todo (LPC_GPIO0->FIOCLR |= (LCD_D5| LCD_D4), LPC_GPIO1->FIOCLR |= (LCD_D7 | LCD_D6))


//#define Delay_1mS for (i=0;i<170;i++){}
//#define Delay_2mS for (i=0;i<300;i++){}
//#define Delay_43mS for (i=0;i<7200;i++){}
//#define Delay_43uS for (i=0;i<8;i++){}


//Public functions
/**
 * @fn		Config_Puerto(void)
 * @brief	It configs the ports that work with the LCD
 */
void Config_Puerto(void);
/**
 * @fn		Enable(void)
 * @brief	It enables the LCD latch
 */
void Enable(void);
/**
 * @fn		IniciarLCD(char ,char , char , char , char , char , char )
 * @brief	It configs the LCD
 * @param	N param
 * @param	F param
 * @param	D param
 * @param	C cursor
 * @param	B
 * @param	ID
 * @param	SH
 */
void IniciarLCD(int N,int F,int D,int C,int B,int ID,int SH);

/**
 * @fn		EsCharLCD(char)
 * @brief	It writes a char into the display
 */
void EsCharLCD(char);

/**
 * @fn		Borrar_Todo(void)
 * @brief	It clears all the display memory
 */
void Borrar_Todo(void);

void Escribir_String(const char *str);

void Borrar_Display();

#endif
