/**
 * @author Vega Maximiliano
 * @file ADS7825_driver.c
 * @brief Drivers para manejo de ADC ADS7825 en forma serie
 * @date 21/09/2014
 * @version	1.0
 * @todo  --------
 * @bug   Podria pinerse qu el pooling saque varias muesttras
 */


/**
 * @note Includes
 */

#include <ADS7825.h>

/**
 * @note Defines
 */

/**
 * @note Variables globales
 */

/**
 * @note Funciones Publicas
 */

/**
 * @fn		OP_RESULT ADS7825_get (int16_t *buffer)
 * @brief	Realiza el inicio d ela conversion y la comunicacion serie
 * @param	buffer : dato obtenido
 * @return	Resultado de la operacion:
 *  END si se ha acabado la conversion y rececion de dato.
 *  NOT_READY si se encuentra en alguno de los procesos
 */
OP_RESULT ADS7825_get (float *buffer){

    int x_buff =  0;

//Si BUSY esta en alto, hay dato disponible
	x_buff = 0;
    //if(get_pin(GPIO_ReadValue(PARALLEL_PORT),PIN_BUSY)){
	if(get_pin(LPC_GPIO2->FIOPIN,PIN_BUSY)){
		//Pido parte alta con el pin BYTE = 0
		//GPIO_ClearValue(PARALLEL_PORT, PIN_BYTE);
		LPC_GPIO2->FIOCLR = PIN_BYTE;

		//x_buff = get_8h( GPIO_ReadValue(PARALLEL_PORT));
		x_buff = get_8h( LPC_GPIO2->FIOPIN );
		//presupongo que cambia el byte mucho mas rapido de lo que el micro puede leer
		//Pido parte baja con el pin BYTE = 1
		//GPIO_SetValue(PARALLEL_PORT, PIN_BYTE);
		LPC_GPIO2->FIOSET = PIN_BYTE;
		
		//x_buff = (x_buff | get_8l( GPIO_ReadValue(PARALLEL_PORT)));
		x_buff = (x_buff | get_8l( LPC_GPIO2->FIOPIN ));

		*buffer = (  ((float)((int16_t) x_buff)) * LSB); //Asigno valor al buffer

		return(END);
	}

return(BUSY);
}


/**
 * @note Funciones Privadas
 */

/**
 * @fn		OP_RESULT GPIO_ADS7825_Init (void)
 * @brief	Inicializacion de GPIO asociado al ADS7825
 * @param   void
 * @return 	Estado de inicializacion
 */
OP_RESULT GPIO_ADS7825_Init (void){

	GPIO_SetDir(RC_PORT,(1<<RC_PIN),SALIDA); // RC es excitado por el micro
	GPIO_SetValue(RC_PORT,(1<<RC_PIN)); //Por default RC en alto (un pulso bajo es el que da el SOC)

	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D0_D8),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D1_D9),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D2_D10),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D3_D11),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D4_D12),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D5_D13),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D6_D14),ENTRADA);
	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_D7_D15),ENTRADA);

	GPIO_SetDir(PARALLEL_PORT,(1<<PIN_BYTE),SALIDA); // BYTE para cambiar de nibble alto o bajo
	GPIO_ClearValue(PARALLEL_PORT, (1<<PIN_BYTE));

	GPIO_SetDir(PARALLEL_PORT, (1<<PIN_BUSY) , ENTRADA); //Entrada de BUSY


	return(OK);
}

/**
 * @fn		OP_RESULT RC_Pulse (void)
 * @brief	Reaizacion del pulso en RC para comenzar la conversion
 * @param	void
 * @return	Resultado de la operacion
 */
inline OP_RESULT	RC_Pulse (void)
{
	//Este pulso deberia ser lo suficientemete lento como para que el ADC vea el pulso negativo
//	GPIO_SetValue(RC_PORT , (1<<RC_PIN));
//
//	GPIO_ClearValue(RC_PORT , (1<<RC_PIN));
//
//	GPIO_SetValue(RC_PORT , (1<<RC_PIN));

	LPC_GPIO0->FIOSET = (1<<RC_PIN);
	LPC_GPIO0->FIOCLR = (1<<RC_PIN);
	LPC_GPIO0->FIOSET = (1<<RC_PIN);
	return(OK);
}







