#include <stdio.h>
#include "statfn.h"
#define N 10


int main (void){
	
	edata medic [10] = {11.331, 11.352, 11.352, 11.341, 11.353, 11.337, 11.342, 11.343, 11.334, 11.338};
	
	printf ("Media: %f \n", media_aritm(medic, N));
	printf ("Desvío: %f \n", desv_std(medic, media_aritm(medic, N), N));
	printf ("Incertidumbre Std: %f \n", incert_std(medic, media_aritm(medic, N), N));
	
	return 0;
	}

