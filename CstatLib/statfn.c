#include "statfn.h"

edata media_aritm(edata* datos, unsigned int n){
	
	edata acum=0;
	int i;
	
		for (i=0; i<n; i++){
			acum+=datos[i];
			}
		acum/=n;
	
	return acum;
	} 

edata var_exp(edata* datos, edata media, unsigned int n){
	
	edata acum=0;	
	int i;
	
		for (i=0; i<n; i++){
			
			acum+=((datos[i]-media)*(datos[i]-media));
			
			}
			
			acum/=(n-1);
			
			
	return acum;
	}
	
edata var_exp_media (edata* datos, edata media, unsigned int n){
	
	edata res;
	res=var_exp(datos, media, n);
	res/=n;
	return res;
	}
	
unsigned int grad_lib (unsigned int n){
	return (n-1);
	}
	
edata desv_std(edata* datos, edata media, unsigned int n){
	
	edata res;
	res=var_exp(datos, media, n);
	if (sizeof(res)==sizeof(double)) return sqrt(res);
	else if  (sizeof(res)==sizeof(float)) return sqrtf(res);
	else if (sizeof(res)==sizeof(long double)) return sqrtl(res); 
	else return ERR;
	
	}
	
edata incert_std (edata* datos, edata media, unsigned int n){
	
	edata res;
	res=desv_std(datos, media, n);
	res/=sqrt(n);
	return res;
	}
	
edata v_desv_std(edata varianza){
	
	if (sizeof(varianza)==sizeof(double)) return sqrt(varianza);
	else if  (sizeof(varianza)==sizeof(float)) return sqrtf(varianza);
	else if (sizeof(varianza)==sizeof(long double)) return sqrtl(varianza); 
	else return ERR;
	}

edata sd_incert_std (edata desv, unsigned int n){
	
	return (desv/sqrt(n));
	}
	
edata v_incert_std (edata varianza, unsigned int n){
	
	edata res;
	res=v_desv_std(varianza);
	res/=sqrt(n);
	return res;
	}
