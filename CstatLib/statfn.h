

typedef double edata;
//edata es un tipo definido con el fin de poder 'acomodar' el tipo en las funciones al requerido 
//(Se puede trabajar con punto flotante de simple o doble precisión cambiando la definición)  

#include <math.h>
#define ERR -1;

edata media_aritm(edata* datos, unsigned int n); 
//Recibe la dirección de inicio del vector de muestas y la cantidad de muestras.
//Devuelve la media aritmética o promedio de las mismas.

edata var_exp(edata* datos, edata media, unsigned int n);
//Recibe la dirección de inicio del vector de muestas, la media aritmética y la cantidad de muestras.
//Devuelve la varianza experimental de las muestras.

edata var_exp_media (edata* datos, edata media, unsigned int n);
//Recibe la dirección de inicio del vector de muestas, la media aritmética y la cantidad de muestras.
//Devuelve la varianza experimental de la muestra. Es decir, la varianza experimental reducida n veces.

unsigned int grad_lib (unsigned int n);
//Calcula los grados de libertad a partir del número de muestras.

edata desv_std(edata* datos, edata media, unsigned int n);
//Recibe la dirección de inicio del vector de muestas, la media aritmética y la cantidad de muestras.
//Devuelve el desvío standard, es decir, la raíz cuadrada de la varianza experimental. 
//Si devuelve -1 existe un error en la definición del tipo 'edata' para el cálculo algorítmico de la raíz cuadrada.

edata incert_std (edata* datos, edata media, unsigned int n);
//Recibe la dirección de inicio del vector de muestas, la media aritmética y la cantidad de muestras.
//Devuelve la incertidumbre standard.

edata v_desv_std(edata varianza);
//Calcula el desvío standard a partir de la varianza.
//Recibe como parámetro la varianza.
//Devuelve el desvío.

edata v_incert_std (edata varianza, unsigned int n);
//Calcula la incertidumbre standard a partir de la varianza.
//Recibe como argumentos la varianza y el número de muestras.
//Devuelve la incertidumbre.

edata sd_incert_std (edata desv, unsigned int n);	 		  	
//Calcula la incertidumbre standard a partir del desvío.
//Recibe como argumentos el desvío y el número de muestras.
//Devuelve la incertidumbre.
