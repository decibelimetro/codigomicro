/**
 * @author	Agustin Alba Chicar
 * @file	tpm.c
 * @brief	Archivo fuente que permite medir tiempo de duracion entre procesos.
 * @date	24/09/2014
 * @version	1.0
 * @todo
 * @bug
 */

/**
 * @note Includes
 */
#include "tpm.h"

/**
 * @note Variables globales
 */
static void *tpmTimer = NULL;

/**
 * @note Funciones publicas
 */

/**
 * @fn tpmConfigureTimer
 * @brief Funcion que configura el timer para merdir el costo de un proceso
 * @param timer puntero a void con el timer
 * @param matchChannel es el canal de match por el que se va a contar
 * @param matchValue es el valor del match del canal
 */
void tpmConfigureTimer(void *timer, uint32_t matchChannel, uint32_t matchValue)
{
	//Estructuras de configuracion del timer
	TIM_TIMERCFG_Type timerCfg;
	//Estructura de configuracion del match
	TIM_MATCHCFG_Type matchCfg;


	//Configuracion del Timer
	timerCfg.PrescaleOption = TIM_PRESCALE_USVAL;
	timerCfg.PrescaleValue = 1;	//Son 1000 us

	//Configuracion del match
	matchCfg.MatchChannel = matchChannel;
	matchCfg.MatchValue = matchValue;
	matchCfg.IntOnMatch = DISABLE;
	matchCfg.ResetOnMatch = ENABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;

	//Inicio el timer
	TIM_Init(timer, TIM_TIMER_MODE, &timerCfg);
	TIM_ConfigMatch(timer, &matchCfg);

	//Guardo el puntero al timer
	tpmTimer = timer;
}

/**
 * @fn tpmStart
 * @brief Arranca el timer
 */
inline void tpmStart()
{
	TIM_Cmd(tpmTimer, ENABLE);
}
/**
 * @fn tpmGetCount
 * @brief	Tomo la cuenta del timer
 * @return	La cuenta del timer
 */
inline uint32_t tpmGetCount()
{
	return (((LPC_TIM_TypeDef *)tpmTimer)->TC - 1);
}
/**
 * @fn tpmStop
 * @brief Frena el timer tomado para la cuenta
 */
inline void tpmStop()
{
	TIM_Cmd(tpmTimer, DISABLE);
}
/**
 * @fn tpmReset
 * @brief Se resetea el timer
 */
inline void tpmReset()
{
	TIM_ResetCounter(tpmTimer);
}

/**
 * @note Funciones Privadas
 */
