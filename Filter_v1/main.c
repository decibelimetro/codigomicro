//
//#include <stdio.h>
//#include <stdlib.h>
//#include <stdint.h>
//#include <string.h>
//#include <math.h>
//
//#include "decFilter.h"
//
////Signal defines
//#define PERIOD          100
//#define N_PERIODS       5
////#define N               (N_PERIODS * PERIOD)
//#define N               512
//
//#define	PI              ((float)3.141592653589793)
//#define TWOPI           (2.0*PI)
//
////Filter defines
//#define	PLANT_Nz_0	0.0
//#define	PLANT_Nz_1	0.095162581964040
//#define	PLANT_Dz_0	1.0
//#define	PLANT_Dz_1	(-0.904837418035960)
//
//
///**
// * @fn printSignal
// * @brief Imprime una señal para el formato de Matlab.
// * @param signal        puntero a la señal a imprimir
// * @param size          numero de elementos de la señal a imprimir
// */
//void printSignal(decFilterType *signal, uint32_t size)
//{
//    uint32_t i;
//    
//    printf("[");
//    for(i = 0; i < size; i++)
//    {
//        if(i < (size -1))
//            printf("%f; ",signal[i]);
//        else
//            printf("%f ",signal[i]);
//    }
//    printf("];\n");
//}
//
//void quantisize(decFilterType *signal, uint32_t size, uint32_t nBits, decFilterType max, decFilterType min)
//{
//    uint32_t i;
//    
//    for(i = 0; i < size; i++)
//    {
//        signal[i] = (max - min) / ((2^nBits) - 1) * signal[i];
//    }
//}
//
//void ejemplo1 ()
//{
//    //Vectores de las señales
//    decFilterType inputSignal[N];
//    decFilterType outputSignal[N];
//    
//    //Vectores del filtro
//    decFilterType numerator[2] = {PLANT_Nz_0, PLANT_Nz_1};
//    decFilterType denominator[2] = {PLANT_Dz_0, PLANT_Dz_1};
//    
//    //Indice
//    uint32_t i;
//    
//    
//    //Creo la pulsada
//    for(i = 0; i < N; i++)
//    {
//        if((i % PERIOD) < (PERIOD/2))
//            inputSignal[i] = 1.0;
//        else
//            inputSignal[i] = 0.0;
//        
//        outputSignal[i] = 0.0;
//    }
//    
//    //Aplico el filtro
//    decFilter_IIR(inputSignal, N, outputSignal, N, numerator, 2, denominator, 2);
//    
//    
//    //Descripcion
//    printf("El circuito muestra como se filtra una señal pulsada cuadrada con un R-C discretizado.\n");
//    printf("R = 1Kohm\n");
//    printf("C1 = 1uF\n");
//    printf("R se conecta en serie con el capacitor C. La entrada es en R y la salida es en C\n");
//    //Imprimo los valores
//    printf("X=");
//    printSignal(inputSignal, N);
//    printf("Y_uC=");
//    printSignal(outputSignal, N);
//    
//    
//    //Genero un delay bloqueante para qeu se impriman todos los vectores.
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);    
//}
//
//void dspDFT_512(float *input, float *output, uint32_t size)
//{
//    uint32_t i, j;
//    float real, imaginary, argument;
//    argument = 2.0 * PI / (float)size;
//    
//    for(i = 0; i < size; i++)
//    {
//        real = 0.0;
//        imaginary = 0.0;
//        argument = 2.0*PI/(float)size;
//        for(j = 0; j < size; j++)
//        {
//            real += cosf(argument *(float)j*(float)i) * input[j];
//            imaginary += sinf(argument *(float)j*(float)i) * input[j];
//        }
//        output[i] = 1.0/(float)size * sqrt(real * real + imaginary * imaginary);
//    }    
//}
//
//void fft(float *data, int nn, int isign)
//{
//    int n, mmax, m, j, istep, i;
//    double wtemp, wr, wpr, wpi, wi, theta;
//    double tempr, tempi;
//    
//    n = nn << 1;
//    j = 1;
//    for (i = 1; i < n; i += 2) {
//	if (j > i) {
//	    tempr = data[j];     data[j] = data[i];     data[i] = tempr;
//	    tempr = data[j+1]; data[j+1] = data[i+1]; data[i+1] = tempr;
//	}
//	m = n >> 1;
//	while (m >= 2 && j > m) {
//	    j -= m;
//	    m >>= 1;
//	}
//	j += m;
//    }
//    mmax = 2;
//    while (n > mmax) {
//	istep = 2*mmax;
//	theta = TWOPI/(isign*mmax);
//	wtemp = sin(0.5*theta);
//	wpr = -2.0*wtemp*wtemp;
//	wpi = sin(theta);
//	wr = 1.0;
//	wi = 0.0;
//	for (m = 1; m < mmax; m += 2) {
//	    for (i = m; i <= n; i += istep) {
//		j =i + mmax;
//		tempr = wr*data[j]   - wi*data[j+1];
//		tempi = wr*data[j+1] + wi*data[j];
//		data[j]   = data[i]   - tempr;
//		data[j+1] = data[i+1] - tempi;
//		data[i] += tempr;
//		data[i+1] += tempi;
//	    }
//	    wr = (wtemp = wr)*wpr - wi*wpi + wr;
//	    wi = wi*wpr + wtemp*wpi + wi;
//	}
//	mmax = istep;
//    }
//}
//
//void createSin(float amplitude, float fSampling, float fSine, uint32_t size, float *buffer)
//{
//	uint32_t i;
//
//	for(i = 0; i < size; i ++)
//	{
//		buffer[i] = amplitude * sinf(2 * PI * fSine * (float)i / fSampling);
//	}
//}
//
//
//void ejemplo2()
//{
//    float inputSignal[N];
//    float outputSignal[N];
//    float outputSignalFFT[2*N + 1];
//    uint32_t i;
//    
//    createSin(1.0, 48000.0, 1000.0, N, inputSignal);
//    
//    //Con DFT
//    //dspDFT_512(inputSignal, outputSignal, N);
//    
//    //Con FFT
//    //Realizo el movimiento de muestras
//    outputSignalFFT[0] = 0.0;
//    for(i = 0; i < N; i++)
//    {
//        outputSignalFFT[2*i + 1] = inputSignal[i];
//        outputSignalFFT[2*i + 2] = 0.0;
//    }
//    fft(outputSignalFFT, 2*N, 1);
//    
//    printf("x=");
//    printSignal(inputSignal, N);
//    printf("X=");
//    //printSignal(outputSignal, N);
//    printSignal(outputSignalFFT, 2*N + 1);
//    
//    //Genero un delay bloqueante para qeu se impriman todos los vectores.
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);
//    for(i = 0; i < 1000000; i++);        
//}
//
///**
// * @fn main
// * @brief Ejecuta un ejemplo de RC discretizado.
// */
//int main(int argc, char** argv) 
//{
//    //ejemplo1();
//    ejemplo2();
//    return (EXIT_SUCCESS);
//}
//



#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define PI	M_PI	/* pi to machine precision, defined in math.h */
#define TWOPI	(2.0*PI)

/*
 FFT/IFFT routine. (see pages 507-508 of Numerical Recipes in C)

 Inputs:
	data[] : array of complex* data points of size 2*NFFT+1.
		data[0] is unused,
		* the n'th complex number x(n), for 0 <= n <= length(x)-1, is stored as:
			data[2*n+1] = real(x(n))
			data[2*n+2] = imag(x(n))
		if length(Nx) < NFFT, the remainder of the array must be padded with zeros

	nn : FFT order NFFT. This MUST be a power of 2 and >= length(x).
	isign:  if set to 1, 
				computes the forward FFT
			if set to -1, 
				computes Inverse FFT - in this case the output values have
				to be manually normalized by multiplying with 1/NFFT.
 Outputs:
	data[] : The FFT or IFFT results are stored in data, overwriting the input.
*/

void fft(float data[], int nn, int isign)
{
    int n, mmax, m, j, istep, i;
    float wtemp, wr, wpr, wpi, wi, theta;
    float tempr, tempi;
    
    //Reordenamiento
    n = nn << 1;
    j = 1;
    for (i = 1; i < n; i += 2) {
	if (j > i) {
	    tempr = data[j];     data[j] = data[i];     data[i] = tempr;
	    tempr = data[j+1]; data[j+1] = data[i+1]; data[i+1] = tempr;
	}
	m = n >> 1;
	while (m >= 2 && j > m) {
	    j -= m;
	    m >>= 1;
	}
	j += m;
    }
    //Calculo
    mmax = 2;
    while (n > mmax) {
	istep = 2*mmax;
	theta = TWOPI/(isign*mmax);
	wtemp = sin(0.5*theta);
	wpr = -2.0*wtemp*wtemp;
	wpi = sin(theta);
	wr = 1.0;
	wi = 0.0;
	for (m = 1; m < mmax; m += 2) {
	    for (i = m; i <= n; i += istep) {
		j =i + mmax;
		tempr = wr*data[j]   - wi*data[j+1];
		tempi = wr*data[j+1] + wi*data[j];
		data[j]   = data[i]   - tempr;
		data[j+1] = data[i+1] - tempi;
		data[i] += tempr; 
		data[i+1] += tempi;
	    }
	    wr = (wtemp = wr)*wpr - wi*wpi + wr;
	    wi = wi*wpr + wtemp*wpi + wi;
	}
	mmax = istep;
    }
    
    for (i = 0; i < nn; i++) 
    {
        data[2*i + 1] /= nn;
        data[2*i + 2] /= nn;
    }
}


void createSin(float amplitude, float fSampling, float fSine, uint32_t size, float *buffer)
{
	uint32_t i;

	for(i = 0; i < size; i ++)
	{
		buffer[i] = amplitude * sinf(2 * PI * fSine * (float)i / fSampling);
	}
}

/**
 * @fn printSignal
 * @brief Imprime una señal para el formato de Matlab.
 * @param signal        puntero a la señal a imprimir
 * @param size          numero de elementos de la señal a imprimir
 */
void printSignal(float *signal, uint32_t size)
{
    uint32_t i;
    
    printf("[");
    for(i = 0; i < size; i++)
    {
        if(i < (size -1))
            printf("%f; ",signal[i]);
        else
            printf("%f ",signal[i]);
    }
    printf("];\n");
}

/**
 * @fn printSignal
 * @brief Imprime una señal para el formato de Matlab.
 * @param signal        puntero a la señal a imprimir
 * @param size          numero de elementos de la señal a imprimir
 */
void printComplexSignal(float *signal, uint32_t size, char * name)
{
    uint32_t i;
    uint32_t midSize = size/2;
    
    printf("%s=[",name);
    for(i = 0; i < midSize; i++)
    {
        printf("%.6f + j*%.6f;", signal[2*i+1], signal[2*i+2]);
    }
    printf("];\n");
}
/********************************************************
* The following is a test routine that generates a ramp *
* with 10 elements, finds their FFT, and then finds the *
* original sequence using inverse FFT                   *
********************************************************/

int main(int argc, char * argv[])
{
	int i;
	int Nx;
	int NFFT;
	float *x;
	float *X;

	/* generate a ramp with 10 numbers */
//	Nx = 10;
//	printf("Nx = %d\n", Nx);
//	x = (double *) malloc(Nx * sizeof(double));
//	for(i=0; i<Nx; i++)
//	{
//		x[i] = i;
//	}
        Nx = 512;
        x = (float *) malloc(Nx * sizeof(float));
        createSin(1.0, 48000.0, 1000.0, Nx, x);

	/* calculate NFFT as the next higher power of 2 >= Nx */
	//NFFT = (int)pow(2.0, ceil(log((double)Nx)/log(2.0)));
        NFFT = 512;
	printf("NFFT = %d\n", NFFT);

	/* allocate memory for NFFT complex numbers (note the +1) */
	X = (float *) malloc((2*NFFT+1) * sizeof(float));

	/* Storing x(n) in a complex array to make it work with four1. 
	This is needed even though x(n) is purely real in this case. */
	for(i=0; i<Nx; i++)
	{
		X[2*i+1] = x[i];
		X[2*i+2] = 0.0;
	}
	/* pad the remainder of the array with zeros (0 + 0 j) */
	for(i=Nx; i<NFFT; i++)
	{
		X[2*i+1] = 0.0;
		X[2*i+2] = 0.0;
	}

	printf("\nInput complex sequence (padded to next highest power of 2):\n");
//	for(i=0; i<NFFT; i++)
//	{
//		printf("x[%d] = (%.2f + j %.2f)\n", i, X[2*i+1], X[2*i+2]);
//	}
        printComplexSignal(X, 2*NFFT + 1, "x");

	/* calculate FFT */
	fft(X, NFFT, 1);

	printf("\nFFT:\n");
//	for(i=0; i<NFFT; i++)
//	{
//		printf("X[%d] = (%.2f + j %.2f)\n", i, X[2*i+1], X[2*i+2]);
//	}
        printComplexSignal(X, 2*NFFT + 1, "X");

	/* calculate IFFT */
	fft(X, NFFT, -1);

	/* normalize the IFFT */
	for(i=0; i<NFFT; i++)
	{
		X[2*i+1] /= NFFT;
		X[2*i+2] /= NFFT;
	}

	printf("\nComplex sequence reconstructed by IFFT:\n");
//	for(i=0; i<NFFT; i++)
//	{
//		printf("x[%d] = (%.2f + j %.2f)\n", i, X[2*i+1], X[2*i+2]);
//	}
        printComplexSignal(X, 2*NFFT + 1, "x");

	getchar();
}