/**
 * @author	Agustin Alba Chicar
 * @file	tpm.h
 * @brief	Es una API para medir tiempo de procesamiento. Utiliza un timer, el match seleccionado y devuelve los resultados en microsegundos
 * @date	23/9/2014
 * @version	1.0
 * @todo
 * @bug
 */

#ifndef TPM_H
#define	TPM_H

/**
 * @note        Includes
 */

//Standard includes
#include <stdint.h>
//CMSIS includes
#include "LPC17xx.h"
#include "lpc17xx_timer.h"


/**
 * @note        Defines
 */
#ifndef ERR
        #define ERR     ((int32_t)-1)
#endif

/**
 * @note        Tipos de datos, estructuras, typdefs
 */


/**
 * @note        Prototipos de funciones publicas
 */
void tpmConfigureTimer(void *timer, uint32_t matchChannel, uint32_t matchValue);
inline void tpmStart();
inline uint32_t tpmGetCount();
inline void tpmStop();
inline void tpmReset();

/**
 * @note        Prototipos de funciones privadas
 */

/**
 * @note        Variables publicas
 */



#endif
