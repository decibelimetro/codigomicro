#ifndef		FLAT_TOP_H
#define		FLAT_TOP_H

/**
 * @var	windowFlatTop
 * @brief	Ventana en tiempo Flat Top para 4094 muestras
 */
extern const float windowFlatTop[];
/**
 * @var	windowFlatTopPower
 * @brief	Potencia de la ventana Flat Top para 4096 muestras
 */
extern const float windowFlatTopPower;

#endif
