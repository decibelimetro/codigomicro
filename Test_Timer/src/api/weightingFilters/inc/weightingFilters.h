#ifndef		WEIGHTING_FILTERS_H
#define		WEIGHTING_FILTERS_H


//Enums

/**
 * @enum		decEWeightingFilters
 * @brief		The available weighting filters available.
 */
typedef enum decEnumWeightingFilters
{
	CURVE_NONE = 0,			/**< Sin curva de ponderacion */
	CURVE_A = 1,			/**< Curve A */
	CURVE_B = 2,			/**< Curve B */
	CURVE_C = 3,			/**< Curve C */
	CURVE_D = 4,			/**< Curve D */
}decEWeightingFilters;

//Variables publicas
/**
 * @var		FiltroA
 * @brief	Filtro de ponderacion de curva A
 */
extern const float FiltroA[];
/**
 * @var		FiltroB
 * @brief	Filtro de ponderacion de curva B
 */
extern const float FiltroB[];
/**
 * @var		FiltroC
 * @brief	Filtro de ponderacion de curva C
 */
extern const float FiltroC[];
/**
 * @var		FiltroD
 * @brief	Filtro de ponderacion de curva D
 */
extern const float FiltroD[];

#endif
