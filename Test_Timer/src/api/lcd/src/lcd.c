#include	"lcd.h"


void Enable(void){
	GPIO_SetValue(0, LCD_EN);     //coloco un "1" en P2.LCD_EN

	Delay_1mS;
	GPIO_ClearValue(0, LCD_EN);   //coloco un "0" en P2.LCD_EN

	Delay_1mS;
	Borrar_Todo(); //deja el bus datos en cero para solo setear 1

}

void EsCharLCD(char Caracter){

	GPIO_SetValue(PORT_RS, LCD_RS); // RS=1

	Borrar_Todo();

	if(Caracter & 0x80)
	{
		GPIO_SetValue(1, LCD_D7);
	}
	if(Caracter & 0x40)
	{
		GPIO_SetValue(1, LCD_D6);
	}
	if(Caracter & 0x20)
	{
		GPIO_SetValue(0, LCD_D5);
	}
	if(Caracter & 0x10)
	{
		GPIO_SetValue(0, LCD_D4);
	}
	Enable();

	//GPIO_SetValue(PORT_BUS, (Caracter & 0x0F) << 5);    //coloco el nibble bajo de caracter en bus de datos (P2.8 ... P2.5)
	if(Caracter & 0x08)
	{
		GPIO_SetValue(1, LCD_D7);
	}
	if(Caracter & 0x04)
	{
		GPIO_SetValue(1, LCD_D6);
	}
	if(Caracter & 0x02)
	{
		GPIO_SetValue(0, LCD_D5);
	}
	if(Caracter & 0x01)
	{
		GPIO_SetValue(0, LCD_D4);
	}
	Enable();

	GPIO_ClearValue(PORT_RS, LCD_RS); // RS=0
}

void IniciarLCD(uint32_t N,uint32_t F, uint32_t D,uint32_t C,uint32_t B,uint32_t ID,uint32_t SH){
    //Power ON
	Delay_43mS;
    //1º paso de secuencia de inicialización.
	GPIO_ClearValue(1, LCD_D7 | LCD_D6);
	GPIO_SetValue(0, LCD_D5 | LCD_D4);
	Enable();

	Delay_1mS;

	//2º paso
	GPIO_ClearValue(0, LCD_D4);
	Enable();

	//3º paso
	GPIO_SetValue(1, (N<<31) | (F<<30) ); //N, F
	Enable();

	Delay_1mS;

	//4º paso
	GPIO_ClearValue(0, LCD_D4);
	GPIO_SetValue(0, LCD_D5);
	Enable();

	//5º paso
	GPIO_SetValue(1, (N<<31) | (F<<30) ); //N, F
	Enable();
	Delay_1mS;

	//6º paso
	GPIO_ClearValue(0, LCD_D5 | LCD_D4);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6 );

	Enable();

	//7º paso
	GPIO_SetValue(0, (C<<26) | (B<<25)); //D, C, B,
	GPIO_SetValue(1, LCD_D7 | (D<<30) );

	Enable();

	Delay_1mS;

	//8º paso
	GPIO_ClearValue(0, LCD_D5 | LCD_D4);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6);

    Enable();

	//9º paso
	GPIO_SetValue(0, LCD_D4);
	Enable();

	Delay_2mS;

	//10º paso
	GPIO_ClearValue(0, LCD_D5 | LCD_D4);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6 );
	Enable();

	//11º paso
	GPIO_SetValue(0, (ID<<26) | (SH<<25)); // I/D, SH
	GPIO_SetValue(1, LCD_D6);
	Enable();

}


void Config_Puerto(void)
{
	// Pines  RS y EN como salidas
	GPIO_SetDir(0, LCD_RS | LCD_EN , 1);
	// Pines de bus de datos D7 a D4 como salidas
	GPIO_SetDir(0, LCD_D5 | LCD_D4, 1);
	GPIO_SetDir(1, LCD_D7 | LCD_D6, 1 );
}


void Borrar_Todo(void)
{	//Maxi: Puse  clear valure o lo que estaba en set value
	//GPIO_ClearValue(PORT_BUS, LCD_D7 | LCD_D6 | LCD_D5 | LCD_D4);
	GPIO_ClearValue(0, LCD_D5 | LCD_D4);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6 );
}

void Escribir_String(const char *str)
{
	uint32_t i;

	//Borramos todo el display
	Borrar_Display();
	//Escribimos todos los caracteres
	for(i = 0; i < 80 && str[i] != '\0'; i++)
	{
		EsCharLCD(str[i]);
	}
}

void Borrar_Display()
{
	//10º paso
	GPIO_ClearValue(0, LCD_D5 | LCD_D4);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6 );
	Enable();

	GPIO_SetValue(0, LCD_D4);
	GPIO_ClearValue(0, LCD_D5);
	GPIO_ClearValue(1, LCD_D7 | LCD_D6 );
	Enable();
}


