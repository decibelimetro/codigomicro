#ifndef		APP_STATE_MACHINE_H
	#define		APP_STATE_MACHINE_H

//Includes

//Standard Includes
#include 	"stdlib.h"
#include 	"stdint.h"

//API Includes
//#include	"delay.h"
#include 	"sonometer.h"
#include	"delay.h"
#include	"lcd.h"
#include 	"utils.h"

//Config includes
#include 	"appConfig.h"


/**
 * @enum	appEState
 * @brief	Es el estado al que se desea ir
 */
typedef enum
{
	NO_ACTION = 0,					/**< No se presiono ningun boton */
	CALIBRATE = 1,					/**< Modo de calibracion del equipo. */
	MEASURE = 2,					/**< Modo de medicion. */
	CURVE = 4,						/**< Modo de seleccion de la curva. */
	CALIBRATE_AND_MEASURE = 3,		/**< Boton de calibracion y medicion */
	CALIBRATE_AND_CURVE = 5,		/**< Boton de calibracion y curva */
	MEASURE_AND_CURVE = 6,			/**< Boton de medicion y curva */
	CALIBRATE_MEASURE_CURVE = 7,	/**< Boton de calibracion, medicion y curva */
}appEState;

/**
 * @def		BUTTON_DEBOUNCE_MASK
 * @brief	Mascara de flags activos
 */
#define		BUTTON_DEBOUNCE_MASK		((uint8_t)0x03)

/**
 * @def		MESSAGE_MAX_LENGTH
 * @brief	Maximo de caracteres que entran en el display
 */
#define		MESSAGE_MAX_LENGTH			80

/**
 * @def		APP_COMMANDS_PORT
 * @brief	Puerto en el que estan los puntos
 */
#define		APP_COMMANDS_PORT			0
/**
 * @def		CALIBRATE_BUTTON_PIN
 * @brief	Pin del boton de calibracion
 */
#define		CALIBRATE_BUTTON_PIN		16
/**
 * @def		MEASURE_BUTTON_PIN
 * @brief	Pin del boton de medicion
 */
#define		MEASURE_BUTTON_PIN			15
/**
 * @def		CURVE_BUTTON_PIN
 * @brief	Pin del boton de curva
 */
#define		CURVE_BUTTON_PIN			17

#define		MESSAGE_CALIBRANDO			((char *)"Calibrando...")
#define		MESSAGE_FIN_CALIBRAR		((char *)"Calibracion                             terminada")
#define		MESSAGE_CURVA				((char *)"Curva: ")
#define		MESSAGE_CURVAS				((char *)"Ninguna-A-B-C-D")
#define		MESSAGE_dBPSL				((char *)" dBPSL")


//Public functions

/**
 * @fn		appStateMachine()
 * @brief	Funcion de la aplicacion en general
 * @details A continuacion se especifica como funciona la aplicacion
 * 4 botones
 * 		Calibrar
 * 		Medir
 * 		Curva
 * 		Iterar
 *
 * Acciones a realizar:
 * 1.- Calibrar
 * 2.- Medir
 * 3.- Seleccionar curva
 *
 * Si aprieta Calibrar:
 * 		Medir N veces, calcular el promedio de potencia y ajustar K, guardar K en la EEPROM
 * 		Informa cuando termina de calibrar
 *
 * Si aprieta Medir:
 * 		Toma la curva, mide e informa valor
 *
 * Si aprieta curva:
 * 		Entra en el menu de seleccion de curvas e itera entre ellas con el mismo boton
 *
 */
void appStateMachine();

//Private functions
/**
 * @fn		appControlStatus()
 * @brief	Cargo el estado de la seleccion de los botones
 * @details	Se rota un 1 cuando esta activo y se rota un 0 cuando no. Luego se busca
 * 			que se matchee una condicion de unos 1 de ceros, para ello aplicamos una
 * 			mascara (MASK_1) que baja los bits que no nos interesan y luego comparamos
 * 			contra el valor que si nos interesa. Cuando matchea es porque esta activo.
 * 			De esta forma podemos testear activaciones por flanco y cambio de nivel.
 * @return	appControlStatus indicando la accion a realizar
 */
appEState appControlStatus();
#endif
