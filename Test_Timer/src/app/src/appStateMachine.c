#include	"appStateMachine.h"

//Public functions

void appStateMachine()
{
	appEState status;
	decEWeightingFilters curve = CURVE_NONE;

	float measurement;
	float calibrationConstant = 1.0;

	//float inputBuffer[SAMPLES];
	float inputBuffer[1];

	uint32_t i;

	//Vector de salida
	char message[80];
	char measureCount[10];

	while(1)
	{
		//Vuelvo a consultar el estado del boton
		status = appControlStatus();

		switch(status)
		{
			case CALIBRATE:
				//Cargo el mensaje de calibracion
				Escribir_String(MESSAGE_CALIBRANDO);
#ifdef	DEBUG_MODE
				printf("%s", MESSAGE_CALIBRANDO);
#endif

				//Se mide N veces y se calcula el promedio al mismo tiempo
				for(i = 0, measurement = 0.0; i < SONOMETER_CALIBRATION_MEASUREMENTS + 1 ; i++)
				{
#ifdef	DEBUG_MODE
					//Creo un seno para probar el algoritmo
					createSin(1.0, 23809.0, 1000.0, SAMPLES, inputBuffer);
#else
					//Realizo las conversiones
					while(decAcquire(inputBuffer, SAMPLES) != FINISHED);
#endif
					//Proceso la potencia
					measurement += decProcessPower(inputBuffer, SAMPLES, decOutputBuffer, SAMPLES + 1, curve);
				}
				measurement /= (float)SONOMETER_CALIBRATION_MEASUREMENTS;

				//Ajusto K
				calibrationConstant = SONOMETER_PRESSURE_POWER_CALIBRATION / measurement;

				//Informo calibracion finalizada
#ifdef	DEBUG_MODE
				printf("Calibracion finalizada. k = %.6f\n", calibrationConstant);
#endif
				//Escribo el fin de la calibracion
				Escribir_String(MESSAGE_FIN_CALIBRAR);
#ifdef	DEBUG_MODE
				printf("%s", MESSAGE_FIN_CALIBRAR);
#endif
				break;

			case MEASURE:
#ifdef	DEBUG_MODE
				//Creo un seno para probar el algoritmo
				createSin(10.0, 23809.0, 1000.0, SAMPLES, inputBuffer);
#else
				//Realizo las conversiones
				while(decAcquire(inputBuffer, SAMPLES) != FINISHED);
#endif
				//Proceso la potencia
				measurement = decProcessPower(inputBuffer, SAMPLES, decOutputBuffer, SAMPLES + 1, curve);
				//Aplico la constante de calibracion
				measurement *= calibrationConstant;
				//Aplico la constante de calibracion
				measurement = decConvertPowerToDBPSL(measurement);

				//Informo calibracion finalizada
#ifdef	DEBUG_MODE
				printf("Medicion = %.6f\n", measurement);
#endif
				//Preparo el mensaje de la curva
				message[0] = '\0';
				sprintf(message, MESSAGE_CURVA);
				//Evaluo el tipo de curva
				switch(curve)
				{
					case CURVE_A:
						strcat(message, "A");
						break;
					case CURVE_B:
						strcat(message, "B");
						break;
					case CURVE_C:
						strcat(message, "C");
						break;
					case CURVE_D:
						strcat(message, "D");
						break;
					default:
						strcat(message, "ninguna");
						break;
				}
				while(strlen(message) != 40)
				{
					strcat(message, " ");
				}

				//Cargo el mensaje de la medicion
				sprintf(measureCount, "%.2f", measurement);
				strcat(message, measureCount);
				strcat(message, MESSAGE_dBPSL);

				//Escribo en el LCD
				Escribir_String(message);
#ifdef	DEBUG_MODE
				printf("%s", message);
#endif

				break;

			case CURVE:
				//Itero en la curva a utilizar
				curve ++;
				if(curve > CURVE_D)
				{
					curve = CURVE_NONE;
				}
				//Informo la nueva curva
#ifdef	DEBUG_MODE
				printf("Curva: %d\n", curve);
#endif
				//Preparo el mensaje de la curva
				message[0] = '\0';
				sprintf(message, MESSAGE_CURVA);
				//Evaluo el tipo de curva
				switch(curve)
				{
					case CURVE_A:
						strcat(message, "A");
						break;
					case CURVE_B:
						strcat(message, "B");
						break;
					case CURVE_C:
						strcat(message, "C");
						break;
					case CURVE_D:
						strcat(message, "D");
						break;
					default:
						strcat(message, "ninguna");
						break;
				}
				for(i = strlen(message); i < 40; i++)
				{
					message[i] = " ";
				}
				message[i] = '\0';

				//Cargo el mensaje de las curvas disponibles
				strcat(message, MESSAGE_CURVAS);

				//Escribo al display
				Escribir_String(message);
#ifdef	DEBUG_MODE
				printf("%s", message);
#endif
				break;
			default:

				break;
		}

		//Aplico una demora
		delay(10);
	}
}

//Private functions

/**
 * @fn		appControlStatus()
 * @brief	Cargo el estado de la seleccion de los botones
 * @details	Se rota un 1 cuando esta activo y se rota un 0 cuando no. Luego se busca
 * 			que se matchee una condicion de unos 1 de ceros, para ello aplicamos una
 * 			mascara (MASK_1) que baja los bits que no nos interesan y luego comparamos
 * 			contra el valor que si nos interesa. Cuando matchea es porque esta activo.
 * 			De esta forma podemos testear activaciones por flanco y cambio de nivel.
 * @return	appControlStatus indicando la accion a realizar
 */
appEState appControlStatus()
{
	static uint8_t btnStatus[3] = {0, 0, 0};
	uint32_t aux;

	//Consulto boton de calibracion
	aux = GPIO_ReadValue(APP_COMMANDS_PORT);
	aux = (aux >> CALIBRATE_BUTTON_PIN);
	aux = (aux & 0x01);
	if(!aux)
	{
		//Paso un 1
		btnStatus[0] = (btnStatus[0] << 1) | 0x01;
	}
	else
	{
		//Paso un 0
		btnStatus[0] = (btnStatus[0] << 1) & 0xFD;
	}

	//Consulto boton de medicion
	aux = GPIO_ReadValue(APP_COMMANDS_PORT);
	aux = (aux >> MEASURE_BUTTON_PIN);
	aux = (aux & 0x01);
	if(!aux)
	{
		//Paso un 1
		btnStatus[1] = (btnStatus[1] << 1) | 0x01;
	}
	else
	{
		//Paso un 0
		btnStatus[1] = (btnStatus[1] << 1) & 0xFD;
	}

	//Consulto boton de curva
	aux = GPIO_ReadValue(APP_COMMANDS_PORT);
	aux = (aux >> CURVE_BUTTON_PIN);
	aux = (aux & 0x01);
	if(!aux)
	{
		//Paso un 1
		btnStatus[2] = (btnStatus[2] << 1) | 0x01;
	}
	else
	{
		//Paso un 0
		btnStatus[2] = (btnStatus[2] << 1) & 0xFD;
	}

	//Cargo el estado del boton a NO_ACTION
	aux = NO_ACTION;
	//Evaluo que boton se presiono de forma correcta y envio la combinacion de botones activos.
	if(btnStatus[0] == BUTTON_DEBOUNCE_MASK)
	{
		aux |= CALIBRATE;
	}
	if(btnStatus[1] == BUTTON_DEBOUNCE_MASK)
	{
		aux |= MEASURE;
	}
	if(btnStatus[2] == BUTTON_DEBOUNCE_MASK)
	{
		aux |= CURVE;
	}

	return aux;
}

