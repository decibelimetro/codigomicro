#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

//#include "ADC.h"
//#include "lpc17xx_gpio.h"
//#include "lpc17xx_timer.h"
//#include "lpc17xx_pinsel.h"
//
//#include "arm_math.h"
//#include "math_helper.h"
//
//#include "dsp.h"
//
//#include "flatTop.h"
//#include "weightingFilters.h"
//
//#include "appResources.h"

//void testADC()
//{
//	uint32_t i;
//
//	//Configuro el ADC
//	adcDevice.ID = ADS7825;
//
//	//Inicializo el ADC
//	adcInit(&adcDevice);
//
//
//	while(1)
//	{
//		//Evaluo el estado de la conversion
//		if(adcGetStatus(&adcDevice) == ADC_READY)
//		{
//			//Comienzo la conversion
//			adcCONVStart(&adcDevice);
//			//Bloqueo hasta tener la conversion del ADC
//			while(adcGetData(&adcDevice) != ADC_END);
//			//Imprimo el valor convertido
//			printf("%f\n\r", adcDevice.BUFFER);
//			for(i = 0; i < 100000; i ++);
//		}
//	}
//}
//
//void testMedicion()
//{
//	float inputBuffer[SAMPLES];
//
//	const float *outputBuffer = 0x2007C000;//[2*SAMPLES];
//
//	float power;
//
//#if(DEBUG_MODE == 0)
//	//Configuro el ADC
//	adcDevice.ID = ADS7825;
//
//	//Realizo conversiones
//	while(decAcquire(inputBuffer, SAMPLES) != FINISHED);
//#else
//	//Creo un seno para probar el algoritmo
//	createSin(1.0, 23809.0, 1000.0, SAMPLES, inputBuffer);
//#endif
//
//	//Proceso la potencia
//	power = decProcessPower(inputBuffer, SAMPLES, outputBuffer, SAMPLES + 1, CURVE_NONE);
//
//	//Imprimo la señal
//	//printFloatSignal(inputBuffer, SAMPLES, "x");
//	//Imprimo la señal
//	printFloatComplexSignal(&(outputBuffer[1]), SAMPLES, "X");
//	printf("Pontecia: %.8f", power);
//
//	while(1);
//}
