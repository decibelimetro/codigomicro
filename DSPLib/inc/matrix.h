/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

#ifndef MATRIX_H
#define	MATRIX_H

/**
 * @note        Includes
 */

//! @note		Standard includes
#include <math.h>
#include <stdint.h>

//! @note		Library includes
#include "dsp.h"

	#ifdef	DSP_MATRIX_ENABLE

	/**
	 * @note        Defines
	 */
	#ifndef ERR
			#define ERR     ((int32_t)-1)
	#endif

	/**
	 * @note        Tipos de datos, estructuras, typdefs
	 */


	/**
	 * @note        Prototipos de funciones publicas
	 */
	void dspMatrix_ScalarMultiplication(float *a, float *b, float *c,  uint32_t size);

	/**
	 * @note        Prototipos de funciones privadas
	 */

	/**
	 * @note        Variables publicas
	 */

	#endif
#endif	/* TEMPLATE_H */
