/**
 * @author	Agustin Alba Chicar
 * @file	dsp.h
 * @brief	Archivo que maneja las incluciones y modulos de la libreria de dsp
 * @date	23/9/2014
 * @version	1.0
 * @todo
 * @bug
 */

#ifndef TEMPLATE_H
#define	TEMPLATE_H

/**
 * @note        Includes
 */

#include <stdint.h>

#include "dspConfig.h"

/**
 * @note        Defines
 */

#ifndef ERR
        #define ERR     ((int32_t)-1)
#endif

#ifndef	PI
	#define	PI		((float)3.141592653589793)
#endif
#ifndef	TWOPI
	#define	TWOPI	(2.0 * PI)
#endif

#include "dft.h"
#include "matrix.h"
#include "fft.h"
#include "power.h"

/**
 * @note        Tipos de datos, estructuras, typdefs
 */


/**
 * @note        Prototipos de funciones publicas
 */

/**
 * @note        Protoitpos de funciones privadas
 */

/**
 * @note        Variables publicas
 */

#endif	/* TEMPLATE_H */
