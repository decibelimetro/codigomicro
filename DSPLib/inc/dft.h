/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

#ifndef DFT_H
#define	DFT_H

/**
 * @note        Includes
 */

//! @note		Standard includes
#include <math.h>
#include <stdint.h>

//! @note		Library includes
#include "dsp.h"

	#ifdef	DSP_DFT_ENABLE

	/**
	 * @note        Defines
	 */

	/**
	 * @note        Tipos de datos, estructuras, typdefs
	 */


	/**
	 * @note        Prototipos de funciones publicas
	 */

	void dspDFT(float *input, float *ouput, uint32_t size);



	/**
	 * @note        Prototipos de funciones privadas
	 */

	/**
	 * @note        Variables publicas
	 */

	#endif
#endif	/* TEMPLATE_H */
