/**
 * @author		Agustin Alba Chicar
 * @file		fft.h
 * @brief		Archivo de cabecera con la implementacion de la FFT. Basado en el libro Numerical Receipes in C.
 * @date		27/09/2014
 * @version		1.0
 * @todo
 * @bug
 */

#ifndef FFT_H
#define	FFT_H

/**
 * @note        Includes
 */

//! @note		Standard includes
#include <math.h>
#include <stdint.h>

//! @note		Library includes
#include "dsp.h"

	#ifdef	DSP_FFT_ENABLE

	/**
	 * @note        Defines
	 */

	/**
	 * @note        Tipos de datos, estructuras, typdefs
	 */


	/**
	 * @note        Prototipos de funciones publicas
	 */
	void dspFFT(float *data, uint32_t size, int32_t algorithm);

	void dspRealFFT(float *data, uint32_t size, int32_t algorithm);

	/**
	 * @note        Prototipos de funciones privadas
	 */

	/**
	 * @note        Variables publicas
	 */

	#endif
#endif	/* FFT_H */
