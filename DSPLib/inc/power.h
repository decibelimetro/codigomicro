/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

#ifndef POWER_H
#define	POWER_H

/**
 * @note        Includes
 */

//! @note		Standard includes
#include <math.h>
#include <stdint.h>

//! @note		Library includes
#include "dsp.h"

	#ifdef	DSP_POWER_ENABLE

	/**
	 * @note        Defines
	 */
	#ifndef ERR
			#define ERR     ((int32_t)-1)
	#endif

	/**
	 * @note        Tipos de datos, estructuras, typdefs
	 */


	/**
	 * @note        Prototipos de funciones publicas
	 */
	float dspPower_Parseval(float *x, uint32_t size);
	float dspPower_ComplexParseval(float *x, uint32_t size);
	float dspPower_Time(float *x, uint32_t size);

	/**
	 * @note        Prototipos de funciones privadas
	 */

	/**
	 * @note        Variables publicas
	 */

	#endif
#endif	/* TEMPLATE_H */
