/**
 * @author		Agustin Alba Chicar
 * @file		dspConfig.h
 * @brief		Archivo de configuracion de la libreria de dsp
 * @date		27/09/2014
 * @version		1.0
 * @todo
 * @bug
 */

#ifndef	DSP_CONFIG_H
#define	DSP_CONFIG_H

#define	DSP_DFT_ENABLE
#define	DSP_MATRIX_ENABLE
#define	DSP_FFT_ENABLE
#define	DSP_POWER_ENABLE

#endif	/* DSP_CONFIG_H */
