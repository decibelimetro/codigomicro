/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

/**
 * @note Includes
 */
#include "matrix.h"

#ifdef	DSP_MATRIX_ENABLE

/**
 * @note Variables globales
 */

/**
 * @note Funciones publicas
 */
/**
 * @fn dspMatrix_ScalarMultiplication
 * @brief Calculo el producto interno de dos vectores.
 * @param a factor
 * @param b factor
 * @param c producto
 * @param size
 */
void dspMatrix_ScalarMultiplication(float *a, float *b, float *c,  uint32_t size)
{
	uint32_t i;

	for(i = 0; i < size; i++)
	{
		c[i] = a[i] * b[i];
	}
}

/**
 * @note Funciones Privadas
 */
#endif
