/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

/**
 * @note Includes
 */
#include "dft.h"

#ifdef	DSP_DFT_ENABLE
/**
 * @note Variables globales
 */


/**
 * @note Funciones publicas
 */

/**
 * @fn dspDFT
 * @brief Calculo la DFT para un vector de 512 muestras
 * @param input
 * @param output
 * @param size
 */
void dspDFT(float *input, float *output, uint32_t size)
{
    uint32_t i, j;
    float real, imaginary, argument;
    argument = TWOPI / (float)size;

    for(i = 0; i < size; i++)
    {
        real = 0.0;
        imaginary = 0.0;
        for(j = 0; j < size; j++)
        {
            real += cosf(argument *(float)j*(float)i) * input[j];
            imaginary += sinf(argument *(float)j*(float)i) * input[j];
        }
        output[i] = 1.0/(float)size * sqrt(real * real + imaginary * imaginary);
    }
}

/**
 * @note Funciones Privadas
 */
#endif
