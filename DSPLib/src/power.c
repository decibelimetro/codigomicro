/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

/**
 * @note Includes
 */
#include "power.h"

#ifdef	DSP_POWER_ENABLE
/**
 * @note Variables globales
 */

/**
 * @note Funciones publicas
 */

/**
 * @fn dspPower_Parseval(float *x, uint32_t size)
 * @brief Calcula por Parseval la potencia de un vector de muestras en el dominio espectral real (ya valor absoluto)
 * @param x muestras
 * @param size tamaño del buffer
 * @return potencia de la señal
 */
float dspPower_Parseval(float *x, uint32_t size)
{
	uint32_t i;
	float power = 0.0;


	for(i = 0; i < size; i++)
	{
		power += x[i] * x[i];
	}
	return power;
}

/**
 * @fn dspPower_ComplexParseval(float *x, uint32_t size)
 * @brief Calcula por Parseval la potencia de un vector de muestras en el dominio espectral complejo
 * @param x muestras
 * @param size tamaño del buffer
 * @return potencia de la señal
 */
float dspPower_ComplexParseval(float *x, uint32_t size)
{
	uint32_t i;
	float power = 0.0;

	for(i = 0; i < size; i++)
	{
		power += (x[i] * x[i]);
	}
	return power;
}

/**
 * @fn getTimePower
 * @brief Obtengo la potencia de una señal en el tiempo
 * @param x buffer
 * @param size numero de muestras
 * @return potencia de la señal
 *
 */
float dspPower_Time(float *x, uint32_t size)
{
	uint32_t i;
	float power = 0.0;

	for(i = 0; i < size; i++)
	{
		power += x[i] * x[i] / (float)size;
	}
	return power;
}

/**
 * @note Funciones Privadas
 */

#endif
