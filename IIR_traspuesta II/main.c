/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>


#define pi 3.141592653589793
#define fo ((double)1000) //Frecuencia de sampleo (una de las constantes debe estar casteada)
#define fs 20000
#define num_taps 7 //Cantidad de coeficientes del numerador
#define den_taps 7//Cantidad de coeficientes del denominador
#define N 320 //Cantidad de muestras

const double NUM[num_taps]={0.475921680572947 , -0.951843361145894 , -0.475921680572947 ,  1.903686722291787 , -0.475921680572947 ,-0.951843361145894 ,  0.475921680572947};
const double DEN[den_taps]={1.000000000000000 , -3.117670628475076 ,  2.992927879632853 , -0.329940981137424 , -0.773399257407412 , 0.153435119109809 ,  0.074648364762186};

double IIR_filter(double*);

int main(void) {

static double signal[N]={0};
volatile uint32_t t=0,i=0; //Variables auxiliars

//----------------Gneracion de señal de prueba---------

	for(t=0;t<N;t++) signal[t]=sin(2*pi*fo*t/fs);

//----------Impresion por pantalla y filtrado---------
	printf("[ ");

	for(i=0;i<N;i++){
		printf(",%lf \n",IIR_filter(&signal[i]));
	}

	printf(" ]\n");

//------------------------------------------------

	for(;;);

    return 0 ;
}


double IIR_filter(double *muestra){

uint32_t k=0,y=0,z=0,x=0; //Variable auxiliar

volatile double acum=0; //Variable de acumulador

static double X[num_taps]={0}; //Linea de demora de entradas
static double Y[den_taps]={0}; //Linea de demora de salidas

static  uint32_t index=0, //Variable de cantidad de muestras ingresadas
		 	     den_aux=0,
		   	     num_aux=0;


	X[0]=*muestra; //Cargo muestra actual

	acum=0; //Limpio acumulador


	if(index>num_taps) num_aux=num_taps; //Veo si han ingresado mas muestras que coeficientes en numerador
	else num_aux=index;

	if(index>den_taps) den_aux=den_taps; //veo si han ingresado mas muestras que coeficientes en denominador
	else den_aux=index;

	//---------------Operatoria--------------------

		for(k=0;k<num_taps;k++) {
			acum+=X[k]*NUM[k]; //MAC de coeficintes del numerador
		}

		for(x=1;x<den_taps;x++) {
			acum-=Y[x-1]*DEN[x]; //MAC de coeficientes del denominador
		}
	//----------Desplazamiento de vectores---------

		for(z=num_aux;z>=1;z--) X[z]=X[z-1];

		for(y=den_aux;y>=1;y--) Y[y]=Y[y-1];

		Y[0]=acum/DEN[0];

		acum=0;

		if(index<N) index++; //Incremento indice, cantidad de muestras que han entrado
		else index=0;
	//----------Asigno nuevo valor de salida--------

return (Y[0]);
}
