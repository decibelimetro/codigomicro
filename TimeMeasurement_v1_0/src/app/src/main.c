/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include "stdint.h"
#include "stdio.h"

#include "lpc17xx_timer.h"
#include <cr_section_macros.h>



int main(void)
{
	uint32_t i = 1000000;
	uint32_t count = 0;

	//Inicio el timer para
	tpmConfigureTimer(LPC_TIM0, 0, 1000000);
	//Inicio el timer
	tpmStart();
	//Proceso
	while(i != 0)
	{
		i--;
	}
	//Freno
	tpmStop();
	//Tomo la cuenta
	count = tpmGetCount();
	//Imprimo la cuenta
	printf("%d", count);

    while(1)
    {
    }

    return 0;
}

