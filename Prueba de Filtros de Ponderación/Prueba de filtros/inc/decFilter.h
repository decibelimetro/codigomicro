/**
 * @author      Agustin Alba Chicar
 * @file        decFilter.h
 * @version     1
 * @brief       Header de decFilter. Contiene las definiciones de tipos y funciones.
 * @todo        
 * @bug
 * @date        24/4/2012
 */

#ifndef DECFILTER_H
#define	DECFILTER_H

/**
 * @note        Includes
 */

#include <stdint.h>

/**
 * @note        Defines
 */

/**
 * @note        Tipos de datos, estructuras, typdefs
 */

/**
 * @typedef decFilterType
 * @brief Sirve para hacer independiente del tipo a las funciones de filtrado        
 */
typedef double decFilterType;


/**
 * @note        Prototipos de funciones publicas
 */

/**
 * @fn decFilter_IIR
 * @brief Funcion que permite aplicar un filtro IIR sobre un vector de entrada dada una salida
 * @param input
 * @param sizeInput
 * @param ouput
 * @param sizeOutput
 * @param numerator
 * @param sizeNumerator
 * @param denominator
 * @param sizeDenominator
 */
void decFilter_IIR(decFilterType *input, uint32_t sizeInput,
                   decFilterType *ouput, uint32_t sizeOutput,
                   decFilterType *numerator, uint32_t sizeNumerator,
                   decFilterType *denominator, uint32_t sizeDenominator);

/**
 * @fn decFilter_FIR
 * @brief Funcion que permite aplicar un filtro FIR sobre un vector de entrada dada una salida
 * @param input
 * @param sizeInput
 * @param ouput
 * @param sizeOutput
 * @param denominator
 * @param sizeDenominator
 */
void decFilter_FIR(decFilterType *input, uint32_t sizeInput,
                   decFilterType *ouput, uint32_t sizeOutput,
                   decFilterType *denominator, uint32_t sizeDenominator);

/**
 * @note        Protoitpos de funciones privadas
 */

/**
 * @note        Variables publicas
 */
#endif	/* DECFILTER_H */

