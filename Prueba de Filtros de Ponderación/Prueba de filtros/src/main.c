/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include "arm_math.h"
#include <math.h>
#include "arm_math.h"
#include "decFilter.h"

// TODO: insert other include files here

// TODO: insert other definitions and declarations here


#define FREC 1000.0f //en Hz
#define FS 48000.0f
#define FFT_LENGHT 512
//Acá hay que tener cuidado porque el algoritmo es circular a partir de Fs/2 pero está delimitado por la longitud de la FFT
//Esto significa que a frecuencias bajas me va a repetir (me va a agregar picos en las frecuencias altas)
//pero si por ejemplo le pongo como entrada un seno de 8k y muestreo a 20k  a la salida no voy a tener repetición cíclica
//y tengo que tener cuidado porque si lo corto al medio pierdo el pico que corresponde a los 8k, ya que el mismo que da
//en la mitad alta del espectro


#define CANT_MUESTRAS 1024
#define LONG_FILTRADA 1050

extern double multitonal [CANT_MUESTRAS];
extern double curva_A_num [7];
extern double curva_A_den [7];

int main(void) {

	SystemInit();

	int i;
	double filtrada[LONG_FILTRADA];
	float32_t espectro[FFT_LENGHT];
	//double senoidal [CANT_MUESTRAS];

	/*for (i=0; i<CANT_MUESTRAS; i++){

		senoidal[i]=arm_sin_f32(2*PI*FREC*(i/FS));
}
*/


	decFilter_IIR(multitonal, CANT_MUESTRAS, filtrada, LONG_FILTRADA, curva_A_num, 7, curva_A_den, 7);
	//decFilter_IIR(senoidal, CANT_MUESTRAS, filtrada, LONG_FILTRADA, curva_A_num, 7, curva_A_den, 7);

 arm_rfft_instance_f32 instancia_rfft;
 arm_cfft_radix4_instance_f32 instancia_cfft;


 arm_rfft_init_f32(&instancia_rfft, &instancia_cfft, FFT_LENGHT, 0, 1);

arm_rfft_f32(&instancia_rfft, filtrada,espectro);

arm_abs_f32(espectro, espectro, FFT_LENGHT);
printf("\nSalida: \n");
for (i=0; i<FFT_LENGHT; i++) printf("%f ",espectro[i]);
printf("\nTerminado");

	while(1) {


    }
    return 0 ;
}

