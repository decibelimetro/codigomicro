################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cr_startup_lpc175x_6x.c \
../src/crp.c \
../src/decFilter.c \
../src/main.c \
../src/signals.c 

OBJS += \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/decFilter.o \
./src/main.o \
./src/signals.o 

C_DEPS += \
./src/cr_startup_lpc175x_6x.d \
./src/crp.d \
./src/decFilter.d \
./src/main.d \
./src/signals.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -D__USE_CMSIS_DSPLIB=CMSISv2p00_DSPLIB_CM3 -D__LPC17XX__ -I"C:\Users\Lucas_2\Documents\LPCXpresso_7.1.1_125\workspace8\Prueba de filtros\inc" -I"C:\Users\Lucas_2\Documents\LPCXpresso_7.1.1_125\workspace8\CMSISv2p00_LPC17xx\inc" -I"C:\Users\Lucas_2\Documents\LPCXpresso_7.1.1_125\workspace8\CMSISv2p00_DSPLIB_CM3\inc" -Og -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


