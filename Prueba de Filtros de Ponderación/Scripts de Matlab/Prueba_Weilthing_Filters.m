%%
% @file 	Prueba_weilthing_filters.m
% @author 	Vega Maximiliano
% @date		07/05/2014
% @version	v1.0
% @todo		TodoText
% @bug		
% @brief	Simulacion de mensurado tipo A
% @details	Excitaci�n mediante poliarmonica bitonal


%% Tareas iniciales
% @note		Borro toda la memoria previa
close all;
clear all;
clc;

% @note		Contador de figuras
fc = 1;

%% Parametros de simulacion
% @note		Parametros del script de prueba
pond='A'; %Ponderaci�n tipo A
fs=16000; %frecuencia de sampling ( para audio)
Ts=1/fs; %per�odo de sampling
A=1; %amplitud de se�al
fo=1000; %frecuencia fundamental de prueba (1KHz para audio)
phase=0; %fase inicial de la se�al de prueba
n_samples=fs/fo; %cantidad de muestras para se�al de prueba
n=3; %cantidad de tonos componentes de la se�al
N=30*n_samples; %cantidad de muestras 
%%HAY QUE DESESTIMAR MUESTRAS POR EL TRANSITORIO, ES UN IIR CON UN
%%TRANSITORIO CONSIDERABLE QUE LA FUNCION FILTE NO ARREGLA

[Num_z,Den_z,H_s]=Aprox_H('A',fs); %obtengo coeficientes del filtro de ponderaci�n

    %-----------------------Grafico de Transferencia Continua-------------------
    bode(H_s);
    grid on;
    %-----------------------Grafcico de transferencia Digital--------------------
    fvtool(Num_z,Den_z); %grafico transferencia discreta
    grid on;
    
    
%% Generacion de se�ales de prueba

S_prueba=GEN_SIN(A,fs,fo,phase,n,N); %genero poliarmonica bitonal
    %----------------------Grafico en tiempo discreto-------------------------
    figure(fc+2);
    subplot(1,2,1);
    %%[row,colum]=SIZE(S_prueba,1);
    aux1=Ts*[0:1:N-1];
    stem(aux1,S_prueba);
    title('Se�al de prueba');
    xlabel('n*Ts [s]');
    ylabel('S_n[n] [V]');
    grid on;

    %----------------------Grafico en frecuencia------------------------------
    subplot(1,2,2)
    FFT_S=2*abs(fft(S_prueba))/N; %normalizo la amplitud
    aux2=linspace(0,fs/2,length(FFT_S(1:end/2))); %creo un vector auxiliar de n_samples/2 desde 0 hasta fs/2
    stem(aux2,FFT_S(1:(end)/2));
    title('Respuesta en frecuencia');
    xlabel('f [Hz]');
    ylabel('�FFT(S_n[n])�');
    grid on;

%% Codigo del script propio de la funcionalidad
% @note     Objeto transferencia Weithing_filter
[S_pond]=filter(Num_z,Den_z,S_prueba); %filtrado de la se�al de 

%% Muestra de resultados
% @note		Graficos, prints, archivos etc.
        %-----------------------Se�al de salida en tiempo-------------------------
        figure(fc+3);
        subplot(1,2,1);
        n_samples=fs/fo;
        aux1=Ts*[0:1:(N-1)];
        stem(aux1,S_pond);
        title('Se�al ponderada');
        xlabel('n*Ts [s]');
        ylabel('S_pond [V]');
        grid on
        
        %-----------------------Espectro de se�al de salida------------------------
        subplot(1,2,2);
        FFT_Spond=2*abs(fft(S_pond))/(N); %normalizo la amplitud
        aux2=linspace(0,fs/2,length(FFT_Spond(1:end/2))); %creo un vector auxiliar de n_samples/2 desde 0 hasta fs/2
        stem(aux2,FFT_Spond(1:(end)/2));
        title('Respuesta en frecuencia de la se�al ponderada');
        xlabel('f [Hz]');
        ylabel('�FFT(S_n[n])�');
        grid on;

        %-----------------------Plano de polos y ceros de H_z------------------
        figure(fc+4);
        zplane(Num_z,Den_z); %grafico plano z de H_z
        grid on;
       
        