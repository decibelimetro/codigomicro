clear all;
close all;
clc;

%trabajo con se�ales de 1024 muestras @ fs=48kHz (aprox. 21ms)
load('signals.mat');

%descomentar para elegir se�al
%signal=sine_440Hz+sine_1k+sine_2k+sine_4k+sine_7500hz;
%signal=square_440Hz;
%signal=sweep;
%signal=white_noise;
signal=long_sweep;
plot(signal);

espectro=abs(fft(signal));
espectro=espectro(1:N/2);
espectro=espectro.*(1/N);
ejefrecuencia= 0:fs/N: fs/2-fs/N;
figure();
plot(ejefrecuencia, espectro);

%agarro los coeficientes del filtro de ponderaci�n desde la funci�n de Maxi

%descomentar para elegir curva:
[num, den, H]=Aprox_H('A', fs);
%[num, den, H]=Aprox_H('B', fs);
%[num, den, H]=Aprox_H('C', fs);
%[num, den, H]=Aprox_H('D', fs);

filtrada= filter(num, den, signal);

%grafico nuevamente la FFTara comparar con la se�al sin filtrar
espectro=abs(fft(filtrada));
espectro=espectro(1:N/2);
espectro=espectro.*(1/N);
ejefrecuencia= 0:fs/N: fs/2-fs/N;
figure();
plot(ejefrecuencia, espectro);
