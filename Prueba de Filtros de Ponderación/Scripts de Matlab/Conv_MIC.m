%%
% @function	Conv_MIC
% @brief	Conversion de mv entregados por MIC a presion en [Pa]y [dB SLP]
% @param    sens_dB:Sensivilidad (leida de manual) [dB]
% @param    x_mv:Muestra de tensi�n entregada por el MIC [mv] 
% @return	S_dB:Potencia sonora (dB Sound Pretion Level)tambien conocido como Lp (presion level) [dB SPL]
% @return   P_Pa:Preci�n [Pa]
% @author	Vega Maximiliano
% @date		Fecha 07/05/2014
% @version	v1.0
% @bug
% @todo	
function [S_dB,P_Pa]=Conv_MIC(x_mv,sens_dB)

Pref_Pa=20*10^(-6); %Pref=20 uPa
sens_mvPa=(10^(sens_dB/20))*10^3;
P_Pa=x_mv/sens_mvPa; %[Pa]=[mv]/[mv/Pa]
S_dB=20*log10(P_Pa/Pref_Pa);

end