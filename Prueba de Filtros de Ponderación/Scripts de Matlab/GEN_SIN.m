%%
% @function	GEN_SIN
% @brief	genera una se�al discreta suma de n sinusoidales de misma frecuencia
% @param	f_n:Frecuencia de la fundamental
% @param    A:Amplitud [V]
% @param    fs:Frecuencia de sampling [Hz]
% @param    phase:fase inicial [rad]
% @paran    n_samples:cantidad de muestras []
% @param    n_tono:numero de tonos []
% @param    N:numero de muestras 
% @return	S_n: vector de 1xn_samples con los valores de la se�al generada
% @author	Vega Maximiliano
% @date		07/05/2014
% @version	v1.3
% @bug      fs/f_n debe ser entero     
% @todo	

function [S_n]=GEN_SIN(A,fs,f_n,phase,n_tono,N)

Ts=1/fs; %per�odo de sampling
n_samples=fs/f_n; %cantidad de muestras
T=[0:Ts:(N-1)*Ts]; %vector tiempo de n_tonos

Signal=zeros(1,N); %vector de 1xn_samples lleno de ceros

    for i=1:n_tono %suma de armonicas con amplitud unitaria 
      Signal=Signal+sin(2*pi*i*f_n*T+phase);  
    end;        

S_n=Signal*A; %multiplico la se�al resultado por la amplitud 

end



