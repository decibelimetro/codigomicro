/**
 * @author
 * @file
 * @brief
 * @date
 * @version
 * @todo
 * @bug
 */

#ifndef TEMPLATE_H
#define	TEMPLATE_H

/**
 * @note        Includes
 */

#include <stdint.h>

/**
 * @note        Defines
 */
#ifndef ERR
        #define ERR     ((int32_t)-1)
#endif

/**
 * @note        Tipos de datos, estructuras, typdefs
 */


/**
 * @note        Prototipos de funciones publicas
 */

/**
 * @note        Protoitpos de funciones privadas
 */

/**
 * @note        Variables publicas
 */



#endif	/* TEMPLATE_H */

