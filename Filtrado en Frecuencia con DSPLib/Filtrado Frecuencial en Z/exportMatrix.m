function exportMatrix(A)
format long;
% get dimensions 
dimensions  = size(A);
N_dim       = length(dimensions);

% print variable declaration to console
fprintf('double[')
for i = 1:N_dim-1
    fprintf(',')
end
% finish declaration and print rows of matrix
fprintf('] A= {%s}\n', exportRow(A, dimensions))

function str_out = exportRow(B, dims)
% recursively print matrix

% init output string
str_out = '';

% have we finished yet?
if length(dims) > 1
    % if not, then go to next layer
    for i=1:dims(1)
        % this test is just to make sure that we do not reshape a
        % one-dimensional array
        if length(dims) > 2
            % print next slice inside curly-braces
            str_out = sprintf('%s{ %s },', str_out, exportRow(reshape(B(i,:), dims(2:end)), dims(2:end)) );
        elseif length(dims) == 2
            % we are almost at the end, so do not use reshape, but stil
            % print in curly braces
            str_out = sprintf('%s{ %s },', str_out, exportRow(B(i,:), dims(2:end)) );
        end
    end
else
    % we have found one of the final layers, so print numbers
    str_out = sprintf('%f, ', B);
    % strip last space and comma
    str_out = str_out(1:end-2);
end
% strip final comma and return
str_out = sprintf('%s', str_out(1:end-1));