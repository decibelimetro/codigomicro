clear all;
close all;

load('signals.mat'); %%cargo se�ales
fs=48000;
N=1024;
%signal=sine_1k;
%signal=long_sweep;
signal=square_440Hz;
plot(signal);
title('Se�al de entrada en tiempo');
[Num_z, Den_z, H_s]=Aprox_H('A', fs);
[num, den]=tfdata(H_s, 'v'); %me llevo el numerador y 
%el denominador de la curva de ponderaci�n posta
H_z=c2d(H_s, 1/fs, 'tustin');

[filtro_z, eje_frec_z]=freqz(Num_z, Den_z);
filtro_z=abs(filtro_z);
figure();
plot(eje_frec_z, filtro_z);
title('filtro en frecuencia discreto');
%genero el eje horizontal de frecuencias. La mitad �til de la FFT
%arranca en 0, va de a saltos fs/N siendo N la longitud de toda la fft
%y la mitad �til termina en fs/2
w=0:(fs/N):(fs/2); %f? 2*i/N=fs/2    0<i<fs/2

w=2*pi*w; %lo paso a pulsaci�n angular w porque la funci�n de abajo me 
%pide los puntos de omega donde quiero evaluar mi respuesta en frecuencia
freqfilter=abs(freqs(num, den, w));

%en las pr�ximas tres l�neas compruebo que el pedazo de curva que tom� en
%un vector para hacer la multiplicaci�n punto a punto sea efectivamente el
%que me interesa compar�ndola con la curva de transferencia del filtro.
figure();
bode(H_s);
figure();
plot(freqfilter);
title('vector del filtro en frecuencia');

%agarro mi se�al y la transformo en frecuencia.
espectro=abs(fft(signal));
espectro=espectro(1:N/2);
espectro=espectro.*(1/N);
ejefrecuencia= 0:fs/N: fs/2-fs/N;
figure();
title('Se�al de entrada en el dominio frecuencial');
plot(ejefrecuencia, espectro);

%acondiciono el vector de filtro para poder multiplicar
freqfilter=freqfilter(2:513);
freqfilter=freqfilter';

%multiplico punto a punto con mi vector de filtro
salida=espectro.*freqfilter;

%filtro ahora con el filtro en z de la misma manera
salida_z=espectro.*filtro_z;

%salida es la salida ya ponderada
figure();
plot(ejefrecuencia, salida);
title('Salida filtrada con el filtro punto a punto');


%salida es la salida ya ponderada
figure();
plot(ejefrecuencia, salida_z);
title('Salida filtrada con el filtro punto a punto pero ahora en Z');



%ahora simplemente hago el filtrado normal en tiempo para comparar
%resultados
timefoutput=filter(Num_z, Den_z, signal);
%miro nuevamente el espectro;
espectro2=abs(fft(timefoutput));
espectro2=espectro2(1:N/2);
espectro2=espectro2.*(1/N);
ejefrecuencia= 0:fs/N: fs/2-fs/N;
figure();
plot(ejefrecuencia, espectro2);
title('Se�al filtrada tradicionalmente en Z');

exportMatrix(filtro_z');
