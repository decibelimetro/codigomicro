%%
% @function	Aprox_H
% @brief	Obtencion de los coeficientes de los weilthing filters
% @param    metodo: metodo de discretización
% @param	curva: tipo de mensurado A, B, C, D [string]
% @param	fs:frecuencia de muestreo [Hz]
% @return   Num_z: coeficientes del numerador de la transferencia discretizada a fs 
% @return   Den_z: coeficientes del denominador de la transferencia discretizada a fs 
% @return   H_s: transferencia en Laplace en formato tf
% @author	Vega Maximiliano
% @date		07/05/2014
% @version	v2.3
% @bug    fs>>Bw PARA AUDIO ESPERANDO UNA RESPUESTA AUDIBLE BW~22 KHz
% @bug    EN NUESTRO CASO BW~8KHz PERO SE MUESTREARA TIPICAMENTE A 44KHz
% @todo	

function [Num_z, Den_z, H_s]=Aprox_H(curva,fs)

Ts=1/fs; %%Frecuencia de sampling

%-------------------Transferencia continua--------------------

    switch(curva)
        case 'A'
            %Los procedimientos aplicados se debieron a la forma en que de
            %la que se disponia la expresion de H(s)
            kA=7.39705*10^9; %contante  
            Z=[0 0 0 0]; %vector de ceros
            P=[-129.4 -129.4 -676.7 -4636 -76655 -76655]; %vector de polos
            H=zpk(Z,P,kA); %armo transferencia expresada en productos de polos y ceros (ZPK)
            [Num_s,Den_s]=zp2tf(Z',P',kA); %transformo de zpk a tf (division de polinomios) y obtengo numerador y denominador
            
              
        case 'B'
            kB=5.99185*10^9;
            Z=[0 0 0 ];
            P=[-129.4 -129.4 -995.9 -76655 -76655];
            H=zpk(Z,P,kB);
            [Num_s,Den_s]=zp2tf(Z',P',kB);

        case 'C'
            kC=5.91797*10;
            Z=[0 0];
            P=[-129.4 -129.4 -76655 -76655];
            H=zpk(Z,P,kC);
            [Num_s,Den_s]=zp2tf(Z',P',kC);

        
        case 'D'
            kD=91104.32;
            A=roots([1 9532 4.0975*10^7]); %obtengo raices de polinomio con raices complejas conjugadas
            Z=[0 A']; %armo vecor de ceros conciderando que roots devuelve una matriz culumna de nx1 debiendo transponerla
            B=roots([1 21514 3.8836*10^8]);
            P=[-1776.3 -7288.5  B'];
            H=zpk(Z,P,kD); %armo transferecnia en forma ZPK
            [Num_s,Den_s]=zp2tf(Z',P',kD); %obtengo transferencia en forma tf 

            
        otherwise
            disp('Curva no contemplada');
            pause;
            clc;
            
    end   

    %------------Discretizacion de la transferencia------------------
         H_s=tf(Num_s, Den_s)
         H_z=c2d(H,Ts,'tustin'); %discretizo utilizando bilinear (Tustin) con prewarp
         [Num_z,Den_z]=tfdata(H_z,'v'); %obtengo coeficientes
         
         
         
end