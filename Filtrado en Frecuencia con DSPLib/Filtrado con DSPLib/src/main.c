/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include "arm_math.h"
#include <math.h>
#include "arm_math.h"
#include "decFilter.h"
#include "dft.h"
#include "matrix.h"
// TODO: insert other include files here

// TODO: insert other definitions and declarations here


#define FREC 1000.0f //en Hz
#define FS 48000.0f
#define FFT_LENGHT 1024
//Acá hay que tener cuidado porque el algoritmo es circular a partir de Fs/2 pero está delimitado por la longitud de la FFT
//Esto significa que a frecuencias bajas me va a repetir (me va a agregar picos en las frecuencias altas)
//pero si por ejemplo le pongo como entrada un seno de 8k y muestreo a 20k  a la salida no voy a tener repetición cíclica
//y tengo que tener cuidado porque si lo corto al medio pierdo el pico que corresponde a los 8k, ya que el mismo que da
//en la mitad alta del espectro


#define CANT_MUESTRAS 1024


//extern float multitonal [CANT_MUESTRAS];
//extern float square_440Hz [CANT_MUESTRAS];
extern float filtropp[512];
extern float long_sweep [CANT_MUESTRAS];


int main(void) {

	SystemInit();

	int i;



/*	for (i=0; i<CANT_MUESTRAS; i++){

		entrada[i]=arm_sin_f32(2*PI*440*(1/48000)*i);
	}
*/
	float espectro[FFT_LENGHT];

	dspDFT(long_sweep, espectro, FFT_LENGHT);


	dspMatrix_ScalarMultiplication(espectro, filtropp, espectro, CANT_MUESTRAS/2);


	//double senoidal [CANT_MUESTRAS];

	/*for (i=0; i<CANT_MUESTRAS; i++){

		senoidal[i]=arm_sin_f32(2*PI*FREC*(i/FS));
}
*/



printf("\nSalida: \n");
for (i=0; i<FFT_LENGHT/2; i++) printf("%f ",espectro[i]);
printf("\nTerminado");

	while(1) {


    }
    return 0 ;
}

