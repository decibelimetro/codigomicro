/**
 * @author      Agustin Alba Chicar 
 * @file        decFilter.h
 * @brief       Implementacion de filtros IIR y FIR
 * @date        25/4/2014
 * @version     1
 * @todo
 * @bug
 */

/**
 * @note Includes
 */
#include "decFilter.h"

/**
 * @note Variables globales
 */


/**
 * @note Funciones publicas
 */
void decFilter_IIR(decFilterType *input, uint32_t sizeInput,
                   decFilterType *output, uint32_t sizeOutput,
                   decFilterType *numerator, uint32_t sizeNumerator,
                   decFilterType *denominator, uint32_t sizeDenominator)
{
    uint32_t i, j;
    decFilterType actual;
    
    for(i = sizeDenominator - 1; i < sizeInput; i++)
    {
        actual = 0.0;
        for(j=0; j < sizeNumerator; j++)
        {
            actual += input[i - sizeNumerator + j + 1] * numerator[j];
            if(j >= 1)
            {
                actual -= output[i - sizeDenominator + j] * denominator[j];
            }
        }
        /*
        for(j=1; j < sizeDenominator; j++)
        {
            actual -= output[i - sizeDenominator + j] * denominator[j];
        }         
         */


        output[i] = actual / denominator[0];
    }
}




void decFilter_FIR(decFilterType *input, uint32_t sizeInput,
                   decFilterType *output, uint32_t sizeOutput,
                   decFilterType *numerator, uint32_t sizeNumerator)
{
    uint32_t i, j;
    decFilterType conv;
    
    for(i = sizeNumerator - 1; i < sizeInput; i++)
    {
        conv = 0.0;
        
        for(j = i - sizeNumerator + 1; j <= i; j++)
        {
            conv += input[j] * numerator[i - j];
        }
        
        output[i - sizeNumerator + 1] = conv;
    }
}

/**
 * @note Funciones Privadas
 */


