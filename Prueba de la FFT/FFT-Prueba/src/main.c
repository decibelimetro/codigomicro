/*
===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include "arm_math.h"
#include <math.h>
#include "arm_math.h"


// TODO: insert other include files here

// TODO: insert other definitions and declarations here


#define FREC 8000.0f //en Hz
#define FS 20000.0f
#define FFT_LENGHT 512
//Acá hay que tener cuidado porque el algoritmo es circular a partir de Fs/2 pero está delimitado por la longitud de la FFT
//Esto significa que a frecuencias bajas me va a repetir (me va a agregar picos en las frecuencias altas)
//pero si por ejemplo le pongo como entrada un seno de 8k y muestreo a 20k  a la salida no voy a tener repetición cíclica
//y tengo que tener cuidado porque si lo corto al medio pierdo el pico que corresponde a los 8k, ya que el mismo que da
//en la mitad alta del espectro


#define CANT_MUESTRAS (FFT_LENGHT)

void GenerarCuadrada(float32_t* signal, int frec_coef);

int main(void) {

	SystemInit();



	float32_t entrada[CANT_MUESTRAS];
	float32_t salida[FFT_LENGHT];

	int i;

//PARA ENTRADA SENOIDAL::
//for (i=0; i<CANT_MUESTRAS; i++) entrada[i]=arm_sin_f32(2*PI*(FREC/FS)*i);
//PARA ENTRADA CUADRADA

	//fcoef= 10 para 1k
	GenerarCuadrada(entrada, 10);
	printf("\nEntrada: \n");
	for (i=0; i<FFT_LENGHT; i++) printf("%f ",entrada[i]);

	arm_rfft_instance_f32 instancia_rfft;
 arm_cfft_radix4_instance_f32 instancia_cfft;


 arm_rfft_init_f32(&instancia_rfft, &instancia_cfft, FFT_LENGHT, 0, 1);

arm_rfft_f32(&instancia_rfft, entrada, salida);

arm_abs_f32(salida, salida, FFT_LENGHT);
printf("\nSalida: \n");
for (i=0; i<FFT_LENGHT; i++) printf("%f ",salida[i]);
printf("\nTerminado");

	while(1) {


    }
    return 0 ;
}

void GenerarCuadrada(float32_t* signal, int frec_coef){

	int i, acum;

	for(i=0; i<CANT_MUESTRAS;){

		for(acum=0; acum<frec_coef; acum++) {
			signal[i]=1;
			i++;
		}

		for(acum=0; acum<frec_coef; acum++) {
			signal[i]=-1;
			i++;
		}

	}

}
