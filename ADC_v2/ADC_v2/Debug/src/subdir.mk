################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ADC_API.c \
../src/ADS7825_driver.c \
../src/cr_startup_lpc175x_6x.c \
../src/crp.c \
../src/main.c 

OBJS += \
./src/ADC_API.o \
./src/ADS7825_driver.o \
./src/cr_startup_lpc175x_6x.o \
./src/crp.o \
./src/main.o 

C_DEPS += \
./src/ADC_API.d \
./src/ADS7825_driver.d \
./src/cr_startup_lpc175x_6x.d \
./src/crp.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCR_PRINTF_CHAR -D__LPC17XX__ -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\Lib_CMSIS\Drivers\include" -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\ADC_v2\inc" -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\CMSISv2p00_LPC17xx\CMSISv2p00_LPC17xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cr_startup_lpc175x_6x.o: ../src/cr_startup_lpc175x_6x.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M3 -D__USE_CMSIS=CMSISv2p00_LPC17xx -DCR_PRINTF_CHAR -D__LPC17XX__ -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\Lib_CMSIS\Drivers\include" -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\ADC_v2\inc" -I"C:\Users\Maximiliano\Google Drive\TD2\Workspace\Workspace_ADC_par\CMSISv2p00_LPC17xx\CMSISv2p00_LPC17xx\inc" -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/cr_startup_lpc175x_6x.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


