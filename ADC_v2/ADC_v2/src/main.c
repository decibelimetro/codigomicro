/*
===============================================================================
 Name        : main.c
 Author      : $(Vega Maximiliano)
 Version     : 1.0
 Copyright   : $(--------)
 Description : main definition
===============================================================================
*/

#include <ADC.h>

int main(void) {

	static ADC_dev my_adc;


	my_adc.ID = ADS7825; //Cargo que periferico voy a utilizar

	ADC_Init(&my_adc); //Inicializo ADC

	for(;;){
		//FIXME: para probar esto es un quilombo, debo reformarlo
			if(ADC_GetStatus(&my_adc) == ADC_READY) CONV_Start(&my_adc); //Si el ADC se encuentra habilitado comienza la conversion
			ADC_GetData(&my_adc);
			}

return 0;

}
