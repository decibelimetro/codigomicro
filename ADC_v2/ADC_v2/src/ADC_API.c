/**
 * @author	Vega Maximiliano
 * @file	AD_API.c
 * @brief	API de manejo de ADC
 * @date	21/09/2014
 * @version	1.0
 * @todo 	--------
 * @bug		--------
 */

/**
 * @note Includes
 */
#include <ADC.h>

/**
 * @note Variables globales
 */

/**
 * @note Funciones publicas
 */

/**
 * @fn		ADC_STATUS ADC_GetStatus (ADC_dev *device)
 * @brief	Obtencion de estado de periferico ADC.
 * @param	*device : Periferico ADC
 * @return   Estado del periferico device
 * ADC_READY si se encuentra listo para conevrtir
 * ADC_BUSY si se encuentra ocupado
 */
ADC_STATUS ADC_GetStatus (ADC_dev *device){

/*
 * Si ya ha convertido el dispositivo etsa listo apra volver a ser iniciado en su conversion
 * de lo contrario no se encuentra listo
 */
if(device->EOC == ON) return(ADC_READY);
	else return(ADC_BUSY);

return(ADC_READY);
}

/**
 * @fn 		int ADC_GetData (ADC_dev *device)
 * @brief	Obtencion de nuevo dato en el periferico ADC device
 * 			Inicia la conversion una ves ejecutada
 * @para	*device : Periferico ADC
 * @return	Estado de la opercaion
 * ADC_BUSY si esta ocupado
 * ADC_END si ha termiando la conversion y la trasnmision y se tiene dato valido en device.BUFFER
 */
ADC_STATUS ADC_GetData (ADC_dev *device){

	switch(device->ID){
	case ADS7825:
				   //Si se ha habilitado/comenzado la conversion
				   if(device->SOC ==ON){
				   if(ADS7825_get(&(device->BUFFER))== END) {
															device -> EOC = ON; //Levanto End Of convertion
															device -> SOC = OFF; //Bajo Start of convertion
					   	   	   	   	   	   	   	   	   	   	return(ADC_END);
				   	}
					else {
						device -> EOC = OFF; //Bajo End of convertion
						return(ADC_BUSY);
					}
				  }
				  break;


	default:
			//FIXME: Deberia avisarse de algun modo que este dispositivo no estsa contemplado
			return(ADC_BUSY);
			break;

	}
return(ADC_BUSY);
}

/**
 * @fn		ADC_STATUS ADC_Init (ADC_dev *device)
 * #brief	Inicializacion de periferico ADC device
 * @param	*device : Perfierico ADC
 * @return  Estado de Inicaiclzacion
 */
ADC_STATUS ADC_Init (ADC_dev *device){

	device->EOC = ON; //Por default comienza con EOC en alto por mas que no haya convertido
	device->SOC = OFF; //Bajo Start of convertion
	device->BUFFER = 0;

	if(GPIO_ADS7825_Init()==OK){

#ifdef __USE_SERIE
	//Si se habilita comunicacion serie se inicializa el SPI
	if(GPIO_ADS7825_Init ()==OK) return (INIT_OK);
#else
	return(INIT_OK);
#endif
	}
		else return(INIT_FAIL);
return(INIT_OK);
}

/**
 * @fn 		ADC_SATUS CONV_Start (ADC_dev *device)
 * @brief	Habilita comienso de conversion
 * @param	device : ADC periferico
 * @return 	Estado del periferico ADC
 */
ADC_STATUS CONV_Start (ADC_dev *device){
	device->SOC = ON; //Levanto Start of convertion
	device->EOC = OFF; //Bajo end of convertion
	//Realizo el pulso en RC apra comenzar conversion
	RC_Pulse();

	return(OK);
}



/**
 * @note Funciones Privadas
 */
