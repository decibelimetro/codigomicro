/**
 * @author Vega Maximiliano
 * @file ADS7825.h
 * @brief Header para drivers de ADS7825
 * @date 21/09/2014
 * @version 1.0
 * @todo ----------
 * @bug -----------
 */

#ifndef ADS7825_H
#define	ADS7825_H

/**
 * @note        Includes
 */

#include <lpc17xx_gpio.h>
#include <stdio.h>

/**
 * @note        Defines
 */

//***********************GPIO***********************

	#define get_pin(port,pin) ((1&(port>>pin)) == 1)? 1:0 //MACRO PARA OBTENER ESTADO DE UN PIN A PARTIR D EUN REGISTRO

	#define get_8h(port) (0xFF & port) //MACRO PARA OBTENER BYTE SUPERIOR DE UN REGISTRO DE 32 BITS
	#define get_8l(port) ((0xFF & port)<<8) //MACRO PARA OBTENER BYTE INFERIOR DE UN REGISTRO DE 32 BITS

	//-----------------Puerto Paralelo------------
	#define PARALLEL_PORT 2 //P2[]
	#define PIN_D0_D8  0 //P2[0]
	#define PIN_D1_D9  1 //P2[1]
	#define PIN_D2_D10 2 //P2[2]
	#define PIN_D3_D11 3 //P2[3]
	#define PIN_D4_D12 4 //P2[4]
	#define PIN_D5_D13 5 //P2[5]
	#define PIN_D6_D14 6 //P2[6]
	#define PIN_D7_D15 7 //P2[7]

	#define PIN_BYTE 12 //P2[12]
	#define PIN_BUSY 11 //P2[11]

	#define RC_PORT 0 //P0
	#define RC_PIN 10 //P0[10]

//**********************GENERAL**********************

	//-------------------GPIO-----------------------
	#define SALIDA 1
	#define ENTRADA 0

	//-----------------OP_RESULT--------------------
	#define OK ((uint8_t) 0xff)
	#define FAIL ((uint8_t) 0xfe)

	#define BUSY ((uint8_t) 0xfa)

	#define NOT_READY ((uint8_t) 0xfd)
	#define READY ((uint8_t) 0xfc)

	#define END ((uint8_t) 0xfb)

	//------------------GENERAL---------------------
	#define MAX_REP ((float) 65536) //(2^16)
	#define FSR ((float) 20) //(+- 10V)
	#define LSB ((float) (FSR/MAX_REP)) //305 uV

/**
 * @note        Tipos de datos, estructuras, typdefs
 */

/**
 * @typedef		OP_RESULT
 * @brief		Resultado de la operacion
 */
typedef uint8_t OP_RESULT;

/**
 * @note        Prototipos de funciones publicas
 */

/**
 * @fn		OP_RESULT ADS7825_get (float *buffer)
 * @brief	FSM que realiza el inicio d ela conversion y la comunicacion serie
 * @param	buffer : Dato obtenido
 * @return	Resultado de la operacion:
 *  END si se ha acabado la conversion y recepcion de dato.
 *  NOT_READY si se encuentra en alguno de los procesos
 */
OP_RESULT ADS7825_get (float *buffer);


/**
 * @note        Prototipos de funciones privadas
 */


/**
 * @fn		OP_RESULT GPIO_ADS7825_Init (void)
 * @brief	Inicializacion de GPIO asociado al ADS7825
 * @param   void
 * @return 	Estado de inicializacion
 */
OP_RESULT GPIO_ADS7825_Init (void);

/**
 * @fn		OP_RESULT RC_Pulse (void)
 * @brief	Reaizacion del pulso en RC para comenzar la conversion
 * @param	void
 * @return	Resultado de la operacion
 */
OP_RESULT	RC_Pulse (void);


#endif	/* ADS7825_H */

