/**
 * @author Vega Maximiliano
 * @file ADC.h
 * @brief Header para manejo de ADC
 * @date 21/09/2014
 * @version 1.0
 * @todo -------
 * @bug Corregir ADC_dev.BUFFER quedaria chico para un ADC de 24 bits
 */

#ifndef ADC_H
#define	ADC_H

/**
 * @note        Includes
 */

#include <stdint.h>

#include <ADS7825.h>

/**
 * @note        Defines
 */

	//-----------------ADC STATUS----------------------
	#define ADC_BUSY ((uint8_t) 0xff)
	#define ADC_READY ((uint8_t) 0xfe)

	#define ADC_END ((uint8_t) 0xfb)

	#define INIT_FAIL ((uint8_t) 0xfd)
	#define INIT_OK   ((uint8_t) 0xfc)

	//-------------------ADC_ID------------------------
	#define NO_ID ((uint8_t) 0xff)

	#define ADS7825 ((uint8_t) 0 )

	//-------------------GENERAL-----------------------
	#define ON 	1
	#define OFF 0

	#define BUFF_LENGTH 10 //Tamaño del buffer

/**
 * @note        Tipos de datos, estructuras, typdefs
 */

/**
 * @typedef 	ADC_ID
 * @brief		Identificador de periferico ADC disponible en MCU
 */
typedef uint8_t ADC_ID;

/**
 * @typedef		ADC_STATUS
 * @brief		Status de peroiferico ADC disponible en MCU
 */
typedef	uint8_t	ADC_STATUS;


/**
 * @struct 		ADC_dev
 * @brief		Estructura de periferico ADC
 * @param		EOC : End of convertion
 * @param		SOC : Start of convertion
 * @param 		BUFFER : Buffer contendor de ultima conversion
 * @param		SIZE_BUFFER : Tamaño del buffer
 * @param		ID : Identificador de ADC a usar
 */
typedef struct{
			   uint8_t EOC; //End of convertion activo alto
			   uint8_t SOC; //Start of convertion
			   float BUFFER; //Buffer que contiene la muestra convertida
			   ADC_ID  ID; //Identificador de ADC a utiliza
			   }ADC_dev;

/**
 * @note        Prototipos de funciones publicas
 */

/**
 * @fn		ADC_STATUS ADC_GetStatus (ADC_dev *device)
 * @brief	Obtencion de estado de periferico ADC.
 * @param	*device : Periferico ADC
 * @return   Estado del periferico device
 */
ADC_STATUS ADC_GetStatus (ADC_dev *device);

/**
 * @fn 		int ADC_GetData (ADC_dev *device)
 * @brief	Obtencion de nuevo dato en el periferico ADC device
 * @para	*device : Periferico ADC
 * @return	Estado del periferico ADC y si hay llegado a EOC (End of convertion y trasnmison)
 */
ADC_STATUS ADC_GetData (ADC_dev *device);

/**
 * @fn		ADC_STATUS ADC_Init (ADC_dev *device)
 * #brief	Inicializacion de periferico ADC device
 * @param	*device : Perfierico ADC
 * @return  Estado de Inicaiclzacion
 */
ADC_STATUS ADC_Init (ADC_dev *device);

/**
 * @fn 		ADC_SATUS CONV_Start (ADC_dev *device)
 * @brief	Habilita comienso de conversion
 * @param	device : ADC periferico
 * @return 	Estado del periferico ADC
 */
ADC_STATUS CONV_Start (ADC_dev *device);


/**
 * @note        Prototipos de funciones privadas
 */



/**
 * @note        Variables publicas
 */



#endif	/* ADC_H */

